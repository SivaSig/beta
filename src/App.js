
import React, { Component } from 'react';
import './App.css';
import Home from './pages/Home';
import SignIn from './pages/SignIn';
import BeyondBusiness from './pages/BeyondBusiness';
import BsasRating  from './pages/BsasRating';
import HowtoHire  from './pages/HowtoHire';
import TalentMarketplace from './pages/TalentMarketplace';
import LeaderShip from './pages/LeaderShip';
import About from './pages/About';
import InvestorRelations from './pages/InvestorRelations';
import Login from './pages/Login';
import TrustSafety from './pages/TrustSafety';
import Contact from './pages/Contact';
import Articles from './pages/Articles';
import BswsLevelSystem from './pages/BswsLevelSystem';
import BSASCertifiedProject from './pages/BSASCertifiedProject';
import HowToFindWork from './pages/HowToFindWork';
import FreelanceWorldWide from './pages/FreelanceWorldWide';
import FreelanceGcc from './pages/FreelanceGcc';
import SelfCertification from './pages/SelfCertification';
import WhyGreenBuilding from './pages/WhyGreenBuilding';
import PropertyResources from './pages/PropertyResources';
import HireInWorld from './pages/HireInWorld';
import HireMiddleEast from './pages/HireMiddleEast';
import GreenBuildingTools from './pages/GreenBuildingTools';
import SiteMap from './pages/SiteMap';
import SmartProProductRatingSystems from './pages/SmartProProductRatingSystems';
import AboutSmartPro from './pages/AboutSmartPro';
import TermsService from './pages/TermsService';
import PrivacyPolicy from './pages/PrivacyPolicy';
import KnowledgeLab from './pages/KnowledgeLab';
import Careers from './pages/Careers';
import Press from './pages/Press';
import Partnering from './pages/Partnering';
import Faqs from './pages/Faqs';
import Community from './pages/Community';
import Header from './components/Header';
import Footer from './components/Footer';
import ScrollToTop from './components/ScrollToTop';
import {BrowserRouter as Router,Route,Switch } from 'react-router-dom';
import Sustainability from './pages/Sustainability';
import SmartCities from './pages/SmartCities';
import BuildingTech from './pages/BuildingTech';
import EnergyCXA from './pages/EnergyCXA';
import GHGClimateChange from './pages/GHGClimateChange';
import HSEFireSafety from './pages/HSEFireSafety';
import EmergingTech from './pages/EmergingTech';
import Form from './components/form/Form';




function App() {
  return (
    <React.Fragment>
      <Router >
    
           <ScrollToTop/>
          <Header/>
          <Form/>
            

          <Switch>
     
{/*           
              <Route   exact path='/' component={Home} />
              <Route  path='/about' component={About} />
              <Route   path='/signin' component={SignIn} />
              <Route  path='/beyondbusiness' component={BeyondBusiness} />
              <Route  path='/bsasrating' component={BsasRating} />
              <Route   path='/talentmarketplace' component={TalentMarketplace} />
              <Route   path='/leadership' component={LeaderShip} />
              <Route   path='/howtohire' component={HowtoHire} />
              <Route   path='/Investorrelations' component={InvestorRelations} />
              <Route   path='/login' component={Login} />
              <Route   path='/trustsafety' component={TrustSafety} />
              <Route   path='/contact' component={Contact} />
              <Route   path='/articles' component={Articles} />
              <Route   path='/bswslevelsystem' component={BswsLevelSystem} />
              <Route   path='/BSAScertifiedproject' component={BSASCertifiedProject} />
              <Route   path='/howtofindwork' component={HowToFindWork} />
              <Route   path='/freelanceworldwide' component={FreelanceWorldWide} />
              <Route   path='/freelancegcc' component={FreelanceGcc} />
              <Route   path='/selfcertification' component={SelfCertification} />
              <Route   path='/whygreenbuilding' component={WhyGreenBuilding} />
              <Route   path='/propertyresources' component={PropertyResources} />
              <Route   path='/hireinworld' component={HireInWorld} />
              <Route   path='/hiremiddleeast' component={HireMiddleEast} />
              <Route   path='/greenbuildingtools' component={GreenBuildingTools} />
              <Route   path='/sitemap' component={SiteMap} />
              <Route   path='/smartproproductratingsystems' component={SmartProProductRatingSystems} />
              <Route   path='/aboutsmartpro' component={AboutSmartPro} />
              <Route   path='/termsservice' component={TermsService} />
              <Route   path='/privacypolicy' component={PrivacyPolicy} />
              <Route   path='/knowledgelab' component={KnowledgeLab} />
              <Route   path='/careers' component={Careers} />
              <Route   path='/press' component={Press} />
              <Route   path='/partnering' component={Partnering} />
              <Route   path='/faqs' component={Faqs} />
              <Route   path='/community' component={Community} />
              <Route   path='/sustainability' component={Sustainability} />
              <Route   path='/smartcities' component={SmartCities} />
              <Route   path='/buildingtech' component={BuildingTech} />
              <Route   path='/energycxa' component={EnergyCXA} />
              <Route   path='/ghgclimatechange' component={GHGClimateChange} />
              <Route   path='/hsefiresafety' component={HSEFireSafety} />
              <Route   path='/Emergingtech' component={EmergingTech} /> */}
          </Switch>

          <Footer/>
      </Router>
           
    </React.Fragment>
  
  );
}

export default App;
