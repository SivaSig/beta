import React, { Component } from 'react'

export default class KnowledgeLab extends Component {
    render() {
        return (
            <div>
               <div  class="Nature-banner inner-page">
         
         <div class="Knowledge-lab">
     
        <div class="patner-trs">
               
                   
                     <div class="container">
                           <div class="row mt-4 text-center">
                            <h2 class="section-title p-tb-20 text-center ">Specialized In Many Areas Ranging From Sustainability,Energy,Environment And Commissioning,All The Way To Upgrade Your Life And Everything In-Between.</h2>
                             <div class="text-center">
                                 <button><a href="">Click here for Freedom</a></button>
                             </div>
    </div>
        </div>          
               
          
          
                 
          </div> 
          </div>


    
 <div class="container mb-4  lab-content">
      <div class="lab">
             <p>ON YOUR WAY TO SUCCESS</p>
           <h2>Latest Courses</h2>
      </div>
              
           <div class="row mb-3 ">
     
           <div class="col-md-4">
                <div class="card-r">
                    <img src="assets/images/lab-1.jpg" alt="smartcity"  />
                    <div class="lab-card-content">
                        
                  
                <h6>Construction Environmental Management Plan</h6>
                <p>July 15, 2020</p>
                  </div>
                </div>
           </div>
             <div class="col-md-4">
                <div class="card-r">
                    <img src="assets/images/lab-1.jpg" alt="smartcity"  />
                     <div class="lab-card-content">
                     <h6>Blockchain Technology</h6>
                     <p>July 15, 2020</p>
                </div>
                 </div>
           </div>
             <div class="col-md-4">
                <div class="card-r">
                    <img src="assets/images/home-6.png" alt="smartcity"  />
                     <div class="lab-card-content">
                 <h6>Digital Transformation</h6>
                     <p>July 15, 2020</p>
                    </div>
                </div>
           </div>
          
</div>
      <div class="row mb-3">
             <div class="col-md-4">
                <div class="card-r">
                    <img src="assets/images/home-5.png" alt="smartcity"  />
                     <div class="lab-card-content">
                  <h6>Artificial Intelligence</h6>
                     <p>July 15, 2020</p>
                      </div>
                </div>
           </div>
             <div class="col-md-4">
                <div class="card-r">
                    <img src="assets/images/home-5.png" alt="smartcity"  />
                     <div class="lab-card-content">
                <h6>Beyond Green Building Technology: Discover Green Building, Basics, Concepts, Energy, BSAS, LEED and More</h6>
                     <p>July 15, 2020</p>
                    </div>
                </div>
           </div>
             <div class="col-md-4">
                <div class="card-r">
                    <img src="assets/images/home-5.png" alt="smartcity"  />
                     <div class="lab-card-content">
                    <h6>Internet Of Things</h6>
                     <p>July 15, 2020</p>
                      </div>
                </div>
           </div>
          
          
          </div>

   
  
           
           
           
           
           
                
                 
               
                 
                 
</div>
</div>
</div>

           
        )
    }
}
