import React, { Component } from 'react'

export default class HowToFindWork extends Component {
    render() {
        return (
            <div>
                  <div  class="Nature-banner inner-page">
           <div class="row mb-4">
               <div class="col-md-6 hire">
                         <div class="hire-text-learn">
                              <h1>Work the way you want</h1>
                         <p>Find the right work for you, with great clients, at the world’s work marketplace.</p>
                         <button><a href="">Create Your Profile</a></button>
                       
                         </div>
                        
                  
               </div>
                 <div class="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/find-work.jpg" alt="img" />
                    
                   
               </div>
           </div>
           
           <div class="container">
     
            <div class="row bmd-layout">
                   <h2 class="section-title p-tb-20 text-center">How it works</h2>
                
               <div class="col-md-4">
                   <img src="assets/images/icons/New specialist.png" alt="icon" />
               </div>
               
                <div class="col-md-8">
                    
                        <h5>Create your prof ile (it’s f ree)</h5>
                       <p>An eye-catching title and client-focused overview help us match you to the work you want. Include your work history, your skills, and your photo. Add more, like an introduction video, to create a profile that really stands out.</p>
               </div>
           </div>
             <div class="row bmd-layout pt-3">
               <div class="col-md-4">
                   <img src="assets/images/icons/New specialist.png" alt="icon" />
               </div>
               
                <div class="col-md-8">
                    
                        <h5>Explore ways to earn</h5>
                        <p>Work and earn in different ways. Bid for jobs. Pitch your projects. Discuss your in-demand skills with our recruiters so they can find opportunities aligned with your passions and career goals. Do all three. Create a predictable pipeline and build your smart network.</p>
                      </div>
           </div>
             <div class="row bmd-layout pt-3 mb-4">
               <div class="col-md-4">
                   <img src="assets/images/icons/New specialist.png" alt="icon" />
               </div>
               
                <div class="col-md-8">
                    
                        <h5>3Get paid securely</h5>
                      <p>Choose how you get paid. Our fixed-price protection releases client payments at project milestones. However you work, our service fees are the same. Spend less time chasing, more earning.</p>
                      </div>
           </div>
           
           <div class="learn-how-work mb-4">
              <button><a href="">Create Your Profile</a></button> 
              
              <h4 class="text-center">Learn as you work</h4>
           </div>
           
           
    </div>
           
              </div>
    

            </div>
        )
    }
}
