import React, {Component} from 'react';

class Home extends Component{
    render(){
        return(
            <div>
               <div className="banner-section">
       <div className="container ">
             <div className="row">
               <div className="col-md-6 smart">
                   <div className="banner-text"  data-aos="fade-right"  data-aos-duration="1500">
                               <div data-aos="fade-down"
     data-aos-easing="linear"
     data-aos-duration="1500">
                              <h1>BEYOND SMART CITIES</h1>
                              </div>
                             
                              <p>Beyond Smart Cities is the India’s work marketplace, connecting millions of Green Budling Professionals ,Energy Engineer & Environment consultants with independent talent around the globe.</p>
                            
                              <form className="d-flex">
                                  <input className="form-control " type="search" placeholder="Search" aria-label="Search" />
                                   <button className="btn btn-outline-success" type="submit">Search</button>
                                </form><br/><br/>
                         
                                <div className="banner btns" data-aos="fade-left"
     data-aos-easing="linear"
     data-aos-duration="1500">
                                <p><a href="Sustainability.php">Sustainability</a></p>
                                <p><a href="smartcities.php">Smart Cities</a></p>
                                <p><a href="buildingtech.php">Building Tech</a></p>
                                <p><a href="energy.php">Energy Tech &CxA</a></p>    
                            </div>
                            <div className="banner btns" className="banner btns" data-aos="fade-left"
     data-aos-easing="linear"
     data-aos-duration="1500">
                            <p><a href="climatechange.php">Climate Change</a></p>
                                <p><a href="firesafety.php">HSE&Fire safety</a></p>
                                <p><a href="emergingtech.php">Emerging Design & Tech</a></p>
                                 
                            </div>

                            <div className="banner btns" className="banner btns" data-aos="fade-left"
     data-aos-easing="linear"
     data-aos-duration="1500">
                            <p><a href="smartProrating.php">Smart-Property Rating</a></p>
                            <p><a href="knowledge-lab.php">Knowledge Lab</a></p>  
                                    
                            </div>
                       
                   </div>
              
                  <div className="col-md-6">
                       <div className="banner-img">
                         
                      </div> 
                   </div>
             </div>
           </div>
     
</div>
</div> 



<section >
  <div className="container">
  
  <div className="row">
 
    <div className="col-md-12">
        <div  data-aos="fade-up"
    >
    <h2>Trusted By</h2>
    </div>
      <div className="section-trusted"  data-aos="fade-up"
     data-aos-duration="3000">
        <div className="trusted">
          <div className="logos">
            <img src="assets/images/l1.png" alt="logos" />
          </div>
         </div>
          <div className="trusted">
          <div className="logos">
            <img src="assets/images/l2.png" alt="logos" />
          </div>
          </div>
          <div className="trusted">
          <div className="logos">
            <img src="assets/images/l3.png" alt="logos" />
          </div>
          </div>
          <div className="trusted">
          <div className="logos">
            <img src="assets/images/l4.png" alt="logos" />
          </div>
          </div>
          <div className="trusted">
          <div className="logos">
            <img src="assets/images/l5.png" alt="logos" />
          </div>
        </div>
       </div>
    </div>
    </div>
  </div>
  
     
    
  </section>
  <section className="Services">
    <div className="container">
        
      <div className="row">
          <div data-aos="fade-up"
     data-aos-duration="2000">
                <h2 className="section-title p-tb-20 text-center">Popular Professional Services</h2>
          </div>
        
             
      
         
                 <div data-aos="fade-up"
     data-aos-duration="3000">
           <div className="carousel-wrap">
  <div className="owl-carousel">
    <div className="item">
        <img src="assets/images/products/Measurement & Verification.jpeg" alt="Beyond Smart Cities" />
        <div className="tittle">
              <h5>Measurement & Verification</h5>
        </div>
      
    
    </div>
     <div className="item"><img src="assets/images/products/LEED Fundamental Commissioning.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>LEED Fundamental Commissioning</h5>
        </div>
      
     
     </div>
     <div className="item"><img src="assets/images/products/Construction Environment Management Plan(CEMP).jpeg" alt="Beyond Smart Cities" />
     <div className="tittle">
              <h5>Construction Environment Management Plan(CEMP)</h5>
        </div>
      
     </div>
       <div className="item"><img src="assets/images/products/Design Review & Technical Services.jpeg" alt="Beyond Smart Cities" />
       <div className="tittle">
              <h5>Design Review & Technical Services</h5>
        </div>
       </div>
     <div className="item"><img src="assets/images/products/Energy Auditing & Management.jpeg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Energy Auditing & Management</h5>
        </div>
     </div>
     <div className="item"><img src="assets/images/products/Environmental Impact Assessments(EIA).jpeg" alt="Beyond Smart Cities" />
     <div className="tittle">
              <h5>Environmental Impact Assessments(EIA)</h5>
        </div>
     </div>
       <div className="item"><img src="assets/images/products/GSAS Crtification for Light Industrial Units.jpeg" alt="Beyond Smart Cities" />
        <div className="tittle">
              <h5>GSAS Crtification for Light Industrial Units</h5>
        </div>
       
       </div>
     <div className="item"><img src="assets/images/products/HVAC Commissioning.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>products/HVAC Commissioning</h5>
        </div>
     </div>
     <div className="item"><img src="assets/images/products/Infrastructure and Road Design Review.jpeg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Infrastructure and Road Design Review</h5>
        </div>
     </div>
       <div className="item"><img src="assets/images/products/GSAS Feasibity Study for Districts and Infrastrucure.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>GSAS Feasibity Study for Districts and Infrastrucure</h5>
        </div>
     </div>
     
      
    </div>
    
    
  </div>
</div>
    </div>  

      <div className="row">
          
                 <div data-aos="fade-up"
     data-aos-duration="3000">
       <h2 className="section-title p-tb-20 text-center">Popular Manufacturers </h2>
          </div>
      
         </div>
                <div data-aos="fade-up"
     data-aos-duration="3000">
            <div className="carousel-wrap">
                 <div className="owl-carousel">
    <div className="item">
        <img src="assets/images/products/Measurement & Verification.jpeg" alt="Beyond Smart Cities" />
        <div className="tittle">
              <h5>Measurement & Verification</h5>
        </div>
      
    
    </div>
     <div className="item"><img src="assets/images/products/commercial-air-conditioning-2571306_1920.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Commercial-air-conditioning</h5>
        </div>
      
     
     </div>
     <div className="item"><img src="assets/images/products/light-bulb-4514505_1920.jpg" alt="Beyond Smart Cities" />
     <div className="tittle">
              <h5>Light-bulb</h5>
        </div>
      
     </div>
       <div className="item"><img src="assets/images/products/led-823383_1920.jpg" alt="Beyond Smart Cities" />
       <div className="tittle">
              <h5>Led</h5>
        </div>
       </div>
     <div className="item"><img src="assets/images/products/carsharing-4382651_1920.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Carsharing</h5>
        </div>
     </div>
     <div className="item"><img src="assets/images/products/architecture-1515491_1920.jpg" alt="Beyond Smart Cities" />
     <div className="tittle">
              <h5>Architecture</h5>
        </div>
     </div>
       <div className="item"><img src="assets/images/products/brick-164650_1280.jpg" alt="Beyond Smart Cities" />
        <div className="tittle">
              <h5>Brick</h5>
        </div>
       
       </div>
     <div className="item"><img src="assets/images/products/bricks-1839553_1920.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Bricks</h5>
        </div>
     </div>
     <div className="item"><img src="assets/images/products/alternative-21761_1920.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Alternative</h5>
        </div>
     </div>
       <div className="item"><img src="assets/images/products/sofa-896576_1920.jpg" alt="Beyond Smart Cities" />
      <div className="tittle">
              <h5>Sofa</h5>
        </div>
     </div>
     
      
    </div>
 
</div>
    </div>  
         
   </div>
        
   
   
  </section>

  
  <section className="talent-section" >
  
      <div className="row">
            <div className="col-md-6 talent-col">
                      
                <div className="product-content pt-5 text-center" data-aos="fade-up"
     data-aos-duration="3000">
          <h6>FOR CLIENTS</h6>
          <h2>FIND TALENT YOUR WAY</h2>
          <p>Work with the largest network of independent professionals and get things done<br/>from quick turnarounds to big transformations.</p>
       
      </div>
      </div>
          <div className="col-md-6">
        <img src="assets/images/home-3.png " alt="Beyond Smart Cities" />
      </div>
      

    </div>

  </section>


<section className="great-work">
 
    <div className="row">
      <div className="col-md-6">
        <img src="assets/images/home-4.png" alt="Beyond Smart Cities" />
      </div>
      <div className="col-md-6">
        <div className="small-content" data-aos="fade-up"
     data-aos-duration="3000">
            <h6>For Talent</h6>
        <h2>Find great work</h2>
        <p>Meet clients you’re excited to work with and take your career or business to new heights.</p>
    
      <hr/>

      <div className="row inner-content">

        <div className="col-md-4">
            <div className="card">
                
           
          <p>Find opportunities for every stage of your freelance career</p>
              </div>
        </div>
        <div className="col-md-4">
               <div className="card">
                
          <p>Control when, where, and how you work</p>
          </div>
        </div>
        <div className="col-md-4">
               <div className="card">
                
          <p>Explore different ways to earn </p>
          </div>
        </div>
        
  
     <br/>
    
    
      </div>
      <br/>
      <div className="">
     <button>Find Opportunities</button>
     </div>
    </div>
  </div>
    </div>
</section > 


<section className="Professional">
    <div data-aos="fade-up"
    >
 
  <h2>Explore The Market Place</h2>
         
    </div>
    <div className="container">
      <div className="row"  data-aos="fade-up"
     data-aos-duration="3000">
          
        <div className="col-md-3">
            <div className="market-place">
                <a href="Sustainability.php">
                     <img src="assets/images/icons/Sustainability _ Environment.png" alt="img" />
                     <h6>Sustainability & Environment </h6>
                     </a>
            </div>
      
        </div>
        <div className="col-md-3">
          <div className="market-place">
               <a href="smartcities.php">
                     <img src="assets/images/icons/Smart Cities _ Infrastructure.png" alt="img" />
                       <h6>Smart Cities</h6>
                        </a>
            </div>
        </div>
        <div className="col-md-3">
           <div className="market-place">
                <a href="buildingtech.php">
                     <img src="assets/images/icons/Building Technology.png" alt="img" />
                     <h6>Building Tech</h6>
                      </a>
            </div>
        </div>
        <div className="col-md-3">
           <div className="market-place">
                <a href="energy.php">
                     <img src="assets/images/icons/Energy _ Commissioning.png" alt="img" />
                     <h6>Energy Tech & CxA </h6>
                      </a>
            </div>
        </div>
     
  
             </div>
                <div className="row" data-aos="fade-up"
     data-aos-duration="3000">
        <div className="col-md-3">
          <div className="market-place">
               <a href="climatechange.php">
                     <img src="assets/images/icons/GHG _ Climate Change.png" alt="img" />
                     <h6>GHG & Climate Change </h6>
                      </a>
            </div>
        </div>
        <div className="col-md-3">
            <div className="market-place">
                 <a href="firesafety.php">
                     <img src="assets/images/icons/HSE _ Fire Safety.png" alt="img" />
                     <h6>HSE and Fire Safety</h6>
                      </a>
            </div>
        </div>
        <div className="col-md-3">
            <div className="market-place">
                 <a href="emergingtech.php">
                     <img src="assets/images/icons/Emerging Design _ Technology.png
" alt="img" />
                     <h6>Emerging Design & Tech</h6>
                      </a>
            </div>
        </div>
        <div className="col-md-3">
          <div className="market-place">
               <a href="smartProrating.php">
                     <img src="assets/images/icons/SmartPro Product Rating Systems.png" alt="img" />
                     <h6>SmartPro Rating  System</h6>
                     </a>
            </div>
        </div>
        </div>
</div>

</section >
<section className="work-your-way text-center">
<div className="container" data-aos="fade-up"
     data-aos-duration="3000">
    <h6>Work Your Way</h6>
    <h2>YOU BRING THE SKILL WE MAKE THE EARNING EASY</h2>
   <br/>
   
    <button>Become A Seller</button>
   <br/>
   <br/>
    <h6>Join Our Growing Freelance Community</h6>
    <h4>How it Works</h4>
  <div className="row pt-4 create-service">
     <div className="col-md-4">
       <div className="content">
       <h5>1.CREATE SERVICE</h5>
       <p>Sign up for free , set up your Gig , and offer your work to our global audience</p>
       </div>
     
     </div>
     <div className="col-md-4">
     <div className="content">
       <h5>2 .DELIVER GREAT WORK</h5>
    <p>Get notified when you get an order and use our system to discuss details with customers</p>
     </div>
</div>
     <div className="col-md-4">
     <div className="content">
       <h5>3 .GET PAID</h5>
     <p>Get paid on time , every time . Payment is transferred to you upon order completion.</p>
     </div>
</div>
  </div>
  <div className="profile-clients">
   <div className="row ">
     <div className="col-md-3">
      <img src="assets/images/image.jpg" alt="Beyond Smart Cities" />
     </div>
     <div className="col-md-3">
      <img src="assets/images/image.jpg" alt="Beyond Smart Cities" />
     </div>
     <div className="col-md-3">
      <img src="assets/images/image.jpg" alt="Beyond Smart Cities" />
     </div>
     <div className="col-md-3">
      <img src="assets/images/image.jpg" alt="Beyond Smart Cities" />
     </div>
    <br/>
     <h2>Learn From Beyond Smart Cities</h2>
     
     <p>On-demand professional courses , led by the world ’ s leading experts .<br/>Discover what it takes to be a top-notch seller on Beyond Smart Cities complimentary Learn from BSC course .</p>
     
    </div>
     <button>Learn More</button>
    </div>
</div>


</section>

<section className="great-work text-center">

   <div className="row">
     <div className="col-md-6">
     <div className="product-content" data-aos="fade-up"
     data-aos-duration="3000">
       <h2>BEYOND SMART PRO RATING FOR PRODUCTS</h2>
       <h4>Going Beyond the Regulatory Equations</h4>
       <p>We help clients make better decisions , integrate sustainability into their business and create innovative solutions . Beyond Smart Cities is dedicated to advancing and catalyzing sustainability in India , Qatar & other part of built environment.</p>
       <br/>
       <button><a href="smartProrating.php">EXPLORE SMART- PRO</a></button>
       <br/>
        <div className="rate">
    <input type="radio" id="star5" name="rate" value="5" />
    <label for="star5" title="text">5 stars</label>
    <input type="radio" id="star4" name="rate" value="4" />
    <label for="star4" title="text">4 stars</label>
    <input type="radio" id="star3" name="rate" value="3" />
    <label for="star3" title="text">3 stars</label>
    <input type="radio" id="star2" name="rate" value="2" />
    <label for="star2" title="text">2 stars</label>
    <input type="radio" id="star1" name="rate" value="1" />
    <label for="star1" title="text">1 star</label>
  </div>
      </div>
     </div>
     <div className="col-md-6">
       <img src="assets/images/home-5.png" alt="Beyond Smart Cities" />
     </div>

</div>
</section>
<section className="products">

   <div className="row">
     <div className="col-md-6">
     <img src="assets/images/home-6.png" alt="Beyond Smart Cities" />
     
     </div>
     <div className="col-md-6">
     <div className="product-content " data-aos="fade-up"
     data-aos-duration="3000">
       <h2>NEW GREEN BUILDING RATING SYSTEM  FOR BUILDING</h2>
       <h4>Going Beyond the Regulatory Equations</h4>
       <br/>
       <br/>
       <img src="assets/images/logo.png" alt="Beyond Smart Cities"  className="product-img"/>
      <br/>
      <br/>
       <br/>
       <button><a href="bsasrating.php">EXPLORE SMART- PRO</a></button>
       <br/>
        <div className="rate">
    <input type="radio" id="star5" name="rate" value="5" />
    <label for="star5" title="text">5 stars</label>
    <input type="radio" id="star4" name="rate" value="4" />
    <label for="star4" title="text">4 stars</label>
    <input type="radio" id="star3" name="rate" value="3" />
    <label for="star3" title="text">3 stars</label>
    <input type="radio" id="star2" name="rate" value="2" />
    <label for="star2" title="text">2 stars</label>
    <input type="radio" id="star1" name="rate" value="1" />
    <label for="star1" title="text">1 star</label>
  </div>
      </div>
    
   </div>
</div>

</section>
            </div>
     
        
        )
    }
}


export default Home;