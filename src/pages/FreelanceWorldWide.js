import React, { Component } from 'react'

export default class FreelanceWorldWide extends Component {
    render() {
        return (
            <div>
                <div  className="Nature-banner inner-page">
         
         <div className="row find-world-banner">
           <div className="col-md-12">
                 <div className="find-world-banner-content">
                     <h1>Find the best specialist jobs</h1>
                     <p>Browse jobs posted on Beyond Smart Cities, or jump right in and create a f ree prof ile to f ind the work that you love to do.</p>
                      <button><a href="">Find Smartwork </a></button>
                 </div>
           </div>
       </div>
          <div className="container">
                <div className="row mb-4 mt-4 hire-world-text">
              <div className="col-md-5">
         
                     <h2 className="section-title h2">Type of Work</h2>
        
                    <ul style={{listStyle:'none'}}>
                        <li><a href="Sustainability.php">Sustainability</a> </li>
                       <li><a href="smartcities.php">Smart Cities</a> </li>
                        <li><a href="buildingtech.php">Building Tech</a> </li>
                         <li><a href="energy.php">Energy &amp; CXA</a> </li>
                          <li><a href="climatechange.php">Climate Change</a> </li>
                           <li><a href="firesafety.php">HSE &amp; Fire Safety</a> </li>
                            <li><a href="emergingtech.php">Emerging Tech</a> </li>
                    </ul>
              </div>
              <div className="col-md-7 text-center">
                    <h2 className="section-title p-tb-20 text-center">Explore remote jobs</h2>
                    
                    <button><a href="">Search for skills like “LEED” or”GSAS”</a></button>
              </div>
          </div>
          </div>
          
       
       </div>
       
            </div>
        )
    }
}
