import React, { Component } from 'react'
import {Link} from 'react-router-dom';
export default class Login extends Component {
    render() {
        return (
            <div>
                <section className="user-area log-in-area">
        <div className="container">
            <div className="user-form-content">
                <div className="row  p-4">
                    <div className="sign-layout ">
                   
                        <div className="login-form-holder position-relative">
                            <div className="form-group text-center">
                                <img src="assets/images/logo.png" alt="logo" />

                                <h4 className="mt-4">Login</h4>
                                
                            </div>
                            <form className="user-form" method="POST" action="index.php">
                              
                                <div className="form-group">
                                    <label>Email Address</label>
                                    <input className="form-control" type="email" name="email" />
                                                                  
                                    </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input className="form-control" type="password" name="password" />
                                         </div>                             
                                <div className="form-group">
                                    <div className="login-action">
                                        <div className="row">
                                            <div className="col-6">
                                                <span className="log-rem">
                                                    <input id="remember-2" type="checkbox" />
                                                    <label className="mobile">Keep me logged in</label>
                                                </span>
                                            </div>
                                            <div className="col-6">
                                                <span className="forgot-login float-end">
                                                    <a className="text-color-blue" href="/">
                                                        Forgot Password?
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div className="col-12">
                                    <div className="form-group">
                                        <button className="btn btn-primary default-btn se-primary-btn" type="submit">
                                            <span className="d-inline-block me-3 se-text-white"><i className="fas fa-arrow-right"></i></span> Login Now
                                        </button>
                                    </div>
                                </div>

                        </form></div>
                        
                        <div className="col-12">
                            <div className="form-group">
                                <p>Don’t Have an Account? <Link to="/signin" className="text-color-orange">Create
                                        One</Link>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
    </section>     
    
            </div>
        )
    }
}
