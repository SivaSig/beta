import React, { Component } from 'react'

export default class EmergingTech extends Component {
    render() {
        return (
            <div>
            <div  class="Nature-banner inner-page">

<div class="row mb-4">
<div class="col-md-12">
<h2 class="section-title p-tb-20 text-center">Emerging Tech</h2>


</div>







</div>
<div class="container-fluid">


<div class="row mb-4">


<div class="col-md-4 articles-btn">

<ul class="navlist-items">
 <h6>Sustainability Specialist Services</h6>
 <li><a href="sustainable-building-design.php">Sustainable Building Design</a></li>
  <li><a href="sustainable-building-design.php">Sustainability Assessments & Reports</a></li>
    <li><a href="sustainable-building-design.php">Sustainability & ESG Strategy Development</a></li>
       <li><a href="sustainable-building-design.php">Corporate Social Responsibility (CSR)</a></li>
   <li><a href="sustainable-building-design.php">Shading Modeling & Analysis</a></li>
    <li><a href="sustainable-building-design.php">Sustainability Health Check</a></li>
     <li><a href="sustainable-building-design.php">Sustainable Procurement</a></li>
 <li><a href="sustainable-building-design.php">Life Cycle Assessment (LCA) </a></li>
   <li><a href="sustainable-building-design.php">Feasibility & Impact Studies</a></li>
   <li><a href="sustainable-building-design.php">Stakeholder Engagement</a></li>
    <li><a href="sustainable-building-design.php">Archaeology & Heritage</a></li>
    
     <h6>Social Sustainability</h6> 
        <li><a href="sustainable-building-design.php">Social Impact Assessment</a></li>
 <li><a href="sustainable-building-design.php">Social Impact Assessment</a></li>
 <h6>Sustainability Strategy & Communications</h6>
   <li><a href="sustainable-building-design.php">Sustainability Reporting Assurance & Advice</a></li>
 <li><a href="sustainable-building-design.php">Strategy Development</a></li>
 <li><a href="sustainable-building-design.php">Environmental Social Governance Due Diligence & Reporting</a></li>
   <li><a href="sustainable-building-design.php">Environmental Approvals & Management Systems</a></li>
    <li><a href="sustainable-building-design.php">National Pollutant Inventory Reporting</a></li>
     <h6>Geosciences</h6>

    <li><a href="sustainable-building-design.php">Geology</a></li>
   <li><a href="sustainable-building-design.php">Ecology</a></li>
    <li><a href="sustainable-building-design.php">Hydrogeology</a></li>
     <li><a href="sustainable-building-design.php">Contamination Assessment & Remediation</a></li>
 <li><a href="sustainable-building-design.php">Geotechnical</a></li>  
  <li><a href="sustainable-building-design.php">Land Quality & Remediation</a></li>
   <li><a href="sustainable-building-design.php">Contaminated Land Studies Services</a></li>
    <li><a href="sustainable-building-design.php">Risk Assessment & Toxicology</a></li>
     <h6>Soil Services</h6>
 <li><a href="sustainable-building-design.php">Field & Sampling</a></li>
   <li><a href="sustainable-building-design.php">Geotechnical Services</a></li>
    <li><a href="sustainable-building-design.php">Laboratory Outsourcing</a></li>
     <li><a href="sustainable-building-design.php">Environmental Data Management</a></li>
 <li><a href="sustainable-building-design.php">Interpretation & Modelling</a></li>
      <li><a href="sustainable-building-design.php">Environmental Assessment & Management</a></li>
 <li><a href="sustainable-building-design.php">Risk Assessment</a></li>
 <h6>Water</h6>
 <li><a href="sustainable-building-design.php">Hydrology & Hydrodynamics</a></li>
   <li><a href="sustainable-building-design.php">Impact Assessment & Permitting</a></li>
    <li><a href="sustainable-building-design.php">Integrated Water Management</a></li>
     <li><a href="sustainable-building-design.php">Wastewater & Stormwater Collection Systems</a></li>
 <li><a href="sustainable-building-design.php">Wastewater Treatment & Recycling</a></li>
 <li><a href="sustainable-building-design.php">Water Treatment & Desalination</a></li>
   <li><a href="sustainable-building-design.php">Water Transmission & Distribution</a></li>
 <li><a href="sustainable-building-design.php">Water Quality</a></li>
     <li><a href="sustainable-building-design.php">Water Audits</a></li>
 <li><a href="sustainable-building-design.php">Marine science</a></li>
 <h6>Air Quality & Noise</h6>
  <li><a href="sustainable-building-design.php">Adverse Amenity</a></li>
   <li><a href="sustainable-building-design.php">Air Quality</a></li>
    <li><a href="sustainable-building-design.php">Building Acoustics</a></li>
     <li><a href="sustainable-building-design.php">Expert Witness</a></li>
 <li><a href="sustainable-building-design.php">CFD</a></li>
 <li><a href="sustainable-building-design.php">Aviation Noise</a></li>
   <li><a href="sustainable-building-design.php">Acoustics & Vibration</a></li>
    <h6>Waste Management</h6>
    <li><a href="sustainable-building-design.php">Waste Minimization Audits</a></li>
     <li><a href="sustainable-building-design.php">Organic Waste Management Plan</a></li>
 <li><a href="sustainable-building-design.php">Supply Chain Management</a></li>
  <li><a href="sustainable-building-design.php">Solid Waste Management Plan</a></li>
     <li><a href="sustainable-building-design.php">Construction Waste Management Plan</a></li>
     <h6>Waste Design & Planning Services</h6>
 <li><a href="sustainable-building-design.php">Waste Design Services</a></li>
 <li><a href="sustainable-building-design.php">Technical Waste Services</a></li>
   <li><a href="sustainable-building-design.php">Waste Transaction Advisory Services</a></li>
    <li><a href="sustainable-building-design.php">Solid Waste Management</a></li>
     <li><a href="sustainable-building-design.php">Waste management strategies</a></li>
 <li><a href="sustainable-building-design.php">Waste to energy solutions</a></li>
 <h6>Environmental Specialist Services</h6>
 <li><a href="sustainable-building-design.php">Planning, Policy & Development </a></li>
 <li><a href="sustainable-building-design.php">Environmental, Social & Governance (ESG) Disclosures</a></li>
  <li><a href="sustainable-building-design.php">Environmental Impact Assessments(EIA)</a></li>
     <li><a href="sustainable-building-design.php">Strategic Environmental Assessment(SEA)</a></li>
 <li><a href="sustainable-building-design.php">Landscape & Visual Impact Assessment</a></li>
<li><a href="sustainable-building-design.php">Environmental Monitoring & Modeling</a></li>
   <li><a href="sustainable-building-design.php">Environmental Site Supervision</a></li>
         
    <li><a href="sustainable-building-design.php">ISO 14001 Environmental management</a></li>
     <li><a href="sustainable-building-design.php">Construction Environment Management Plan(CEMP)</a></li>
 <li><a href="sustainable-building-design.php">Standards, KPI & Framework Development</a></li>
       <h6>Environmental Management, Planning & Approvals</h6>
            <li><a href="sustainable-building-design.php">Community Planning</a></li>
  <li><a href="sustainable-building-design.php">Policy Conception & Implementation</a></li>
     <li><a href="sustainable-building-design.php">Occupational Hygiene</a></li>
  <li><a href="sustainable-building-design.php">Environmental Management, Permitting, & Compliance</a></li>
 <li><a href="sustainable-building-design.php">Landscape Architecture</a></li>
   <li><a href="sustainable-building-design.php">Eco-Reinforcement</a></li>
    <li><a href="sustainable-building-design.php">Oil Spill Prevention, Preparedness, & Response Plans</a></li>
           
</ul>
</div>
<div class="col-md-8">
<div class="row  Sustainable-inner-design">
<div class="col-md-4">
<a href="sustainable-building-design.php">


<div class="card-r">
<img src="assets/images/products/Measurement &amp; Verification.jpeg" alt="img" />
<h6>Sustainable Building Design</h6>
</div>
</a>
</div>
<div class="col-md-4">
<a href="sustainable-building-design.php">
<div class="card-r">
<img src="assets/images/products/Design Review &amp; Technical Services.jpeg" alt="img" />
<h6>Sustainability Assessments & Reports</h6>
</div>
</a>
</div>
<div class="col-md-4">
<a href="sustainable-building-design.php">
<div class="card-r">
<img src="assets/images/products/Environmental Impact Assessments(EIA).jpeg" alt="img" />
<h6>Sustainability & ESG Strategy Development</h6>
</div>
</a>
</div>
</div>
<div class="row  Sustainable-inner-design">
<div class="col-md-4">
<a href="sustainable-building-design.php">


<div class="card-r">
<img src="assets/images/products/Measurement &amp; Verification.jpeg" alt="img" />
<h6>Sustainable Building Design</h6>
</div>
</a>
</div>
<div class="col-md-4">
<a href="sustainable-building-design.php">
<div class="card-r">
<img src="assets/images/products/Design Review &amp; Technical Services.jpeg" alt="img" />
<h6>Sustainability Assessments & Reports</h6>
</div>
</a>
</div>
<div class="col-md-4">
<a href="sustainable-building-design.php">
<div class="card-r">
<img src="assets/images/products/Environmental Impact Assessments(EIA).jpeg" alt="img" />
<h6>Sustainability & ESG Strategy Development</h6>
</div>
</a>
</div>
</div>
<div class="row  Sustainable-inner-design">
<div class="col-md-4">
<a href="sustainable-building-design.php">


<div class="card-r">
<img src="assets/images/products/Measurement &amp; Verification.jpeg" alt="img" />
<h6>Sustainable Building Design</h6>
</div>
</a>
</div>
<div class="col-md-4">
<a href="sustainable-building-design.php">
<div class="card-r">
<img src="assets/images/products/Design Review &amp; Technical Services.jpeg" alt="img" />
<h6>Sustainability Assessments & Reports</h6>
</div>
</a>
</div>
<div class="col-md-4">
<a href="sustainable-building-design.php">
<div class="card-r">
<img src="assets/images/products/Environmental Impact Assessments(EIA).jpeg" alt="img" />
<h6>Sustainability & ESG Strategy Development</h6>
</div>
</a>
</div>
</div>


</div>
</div>
</div>

</div>
</div>
        )
    }
}
