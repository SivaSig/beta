import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class HowtoHire extends Component {
    render() {
        return (
            <div>
                
     <div  className="Nature-banner inner-page">
           <div className="row mb-4">
               <div className="col-md-6 hire">
                         <div className="hire-text">
                              <h1>Assess the Skills  Needed for Your Ideal Job</h1>
                         <p>Find f reelancers and run projects your way at the world’s work marketplace.</p>
                         <button><a href="">Find Specialist</a></button>
                         <br/>
                         <br/>
                        <p> Looking to get hired?.<Link to="/signin" className="text-color-orange" >Sign up here</Link>
                                </p> 
                         </div>
                        
                  
               </div>
                 <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/hire-1.jpg" alt="img"/>
                    
                   
               </div>
           </div>
         <div className="container">
        
               <div className="row">
                   <div className="col-md-12 text-center">
                         <h2 className="section-title  text-center">Let’s get to work</h2>
                         <p>Build relationships and create your own Virtual Talent Base for quick project turnarounds or big transformations</p>
                         
                   </div>
               </div>
                 <div className="row mb-4">
                     <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/hire-2.jpg" alt="img"/>
                    
                   
               </div>
                     
               <div className="col-md-6 hire">
                         <div className="hire-text-section2">
                              <h2>Post a job and hire a Specialist</h2>
                            <p>Connect with specialist that gets you with Talent MarketplaceFind f reelancers and run projects your way at the world’s work marketplace</p>
                            <p>Post your work on the world’s work marketplace and wait for the proposals to flood in f rom talented people around the world.</p>
                            <p>Our advanced algorithms help you shortlist candidates who are the best f it. And you can check prof iles, portfolios, and reviews before you give someone the green light.</p>
                         
                         <button><a href="">Find Specialist </a></button>
                        
                         </div>
                        
                  
               </div>
                 
           </div>
               <div className="row mb-4">
                   
                      <div className="col-md-6 hire">
                         <div className="hire-text-section2">
                              <h2>SmartPro Rated Products</h2>
                              <p>SmartPro is a product certification which helps an environmentally conscious customer to make an informed choice to buy eco-f riendly products.</p>
                             <p>SmartPro is a mark of guarantee that the product which bears the SmartPro label is environment f riendly throughout its life cycle.</p>
                          <p>SmartPro ultimately empowers a customer with the knowledge of the product and steer towards sustainable products.</p>
                         <button><a href="">Find SmartPro Products </a></button>
                         <br/>
                         <br/>
                         
                        <p> Looking to get hired?.<Link to="/signin" className="text-color-orange" >Sign up here</Link>
                                </p> 
                         </div>
                        
                  
               </div>
                     <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/hire3.jpeg" alt="img" />
                    
                   
               </div>
                     
            
                 
           </div>
                <div className="row mb-4">
                     <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/hire4.jpeg" alt="img" />
                    
                   
               </div>
                     
               <div className="col-md-6 hire">
                         <div className="hire-text text-center">
                              <h2>Our buildings can create a healthier, more sustainable future.</h2>
                             <p>BSAS(Beyond Sustainability Assessment System)world leading green building project and performance management system. It delivers a comprehensive framework for green building design, construction, operations and performance.</p>
                          <h5>It’s free</h5>
                         <button><a href="">Register Your Property </a></button>
                        
                         </div>
                        
                  
               </div>
                 
           </div>
          <div className="row mb-4">
                   
                      <div className="col-md-6 hire">
                         <div className="hire-text-section2">
                              <h2>All in one place </h2>
                            <p>Once you sign in you’ll get your own online space to manage your project. You’re safe with usYou get what you pay for. And we can prove it.</p>
                         <p>Use it to securely send and receive files, give real-time feedback and make payments.</p>
                         
                         <button><a href="">Find SmartPro Products </a></button>
                         <br/>
                         <br/>
                         
                        <p> Looking to get hired?.<Link to="/signin" className="text-color-orange" >Sign up here</Link>
                                </p> 
                         </div>
                        
                  
               </div>
                     <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/hire3.jpeg" alt="img" />
                    
                   
               </div>
                     
            
                 
           </div>
         
         </div>
            </div>
    
            </div>
        )
    }
}
