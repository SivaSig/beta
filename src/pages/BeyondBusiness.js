import React, { Component } from 'react'

export default class BeyondBusiness extends Component {
    render() {
        return (
            <div>
                
     <div  className="Nature-banner inner-page">
           <div className="row mb-4">
               <div className="col-md-6 ">
                         <div className="beyond-text-section2 pro">
                              <h2>Boost your <br/>business with <br/>Specialist Pro</h2>
                         <p>Set your team up for success with f reelance talent and advanced tools for everyday projects of any scope.</p>
                         <button><a href="">Create Account</a></button>
                         
                         </div>
                        
                  
               </div>
                 <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/beyand-bussines.jpeg" alt="img" />
                    
                   
               </div>
           </div>
         <div className="container">
        
               <div className="row beyond-business-layout2">
                   <div className="col-md-12 text-center">
                         <h2 className="section-title  text-center">Access a catalog of verif ied, trusted Specialis</h2>
                         <p>Expand your team as needed with experienced specialists already vetted for business projects.</p>
                         <div>
                             <button><a href="">Join Now</a></button> 
                         </div>
                        
                   </div>
               </div>
                 <div className="row mt-4 mb-4">
                      <div className="col-md-6 ">
                         <div className="beyond-text-section2">
                              <h2>Promoting <br/>Sustainability by <br/>Marketing <br/>Green Products</h2>
                              <p>Ready to join the sustainable movement.</p>
                              <div className="text-center pb-4">
                                   <button><a href="">Create SmartPro Account </a></button>
                        
                                  
                              </div>
                        
                         </div>
                        
                  
               </div>
                     <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/business.png" alt="img" />
                    
                   
               </div>
                     
              
                 
           </div>
           <div className="row mb-4">
               <div className="col-md-12">
                    <h2 className="section-title  text-center">Going Green</h2>
                    <p>By promoting your brand with eco-f riendly promotional products, your company is taking part in a revolutionary movement that defines this generation. Decrease your company's ecological footprint and increase brand awareness with sustainable promotional products you can feel good about giving away.Sustainability may have started out as a consumer trend. “Going green” was once the cool thing to do. Now - it’s the expected thing to do.</p>
                  <div className="text-center">
                      <button><a href="">Join Now</a></button>
                  </div>
               
               </div>
           </div>
               <div className="row mt-4 mb-4">
                      <div className="col-md-6 ">
                         <div className="beyond-text-section2 rating">
                              <h2>Boost your business<br/> with BSAS Property<br/> Rating System</h2>
                              <p>We can’t just consume our way to a more sustainable world.</p>
                              <div className="text-center pb-4">
                                   <button><a href="">Create Account </a></button>
                        
                                  
                              </div>
                        
                         </div>
                        
                  
               </div>
                     <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/beyond-business-4.jpeg" alt="img" />
                    
                   
               </div>
                     
              
                 
           </div>
             <div className="row mb-4">
               <div className="col-md-12">
                    <h2 className="section-title  text-center">BSAS is the next generation standard for green building design, construction, operations and performance</h2>
                 <p>Green buildings are the foundation of something bigger: helping people, and the communities and cities they reside in—safely, healthily, and sustainably thrive.</p>
                  <div className="text-center">
                      <button><a href="">Join Now</a></button>
                  </div>
               
               </div>
           </div>
     
         </div>
            </div>
    
            </div>
        )
    }
}
