import React, { Component } from 'react'

export default class InvestorRelations extends Component {
    render() {
        return (
            <div>
                 <div  className="Nature-banner inner-page">
         
         <div className="row Investor-banner">
           <div className="col-md-12">
                 <div className="Investor-banner-content">
                     <h1>Investor Relations</h1>
                    
                 </div>
           </div>
       </div>
       
       
       
       
       
    <div className="container">
            <div className="row mb-4">
          <div className="col-md-12">
              <h2 className="section-title p-tb-20 text-center">Company Overview</h2>
        
              
           
               <p>Beyond Smart Cities is changing the way businesses find, hire, and pay highly skilled service provider for short-term and longer-term projects. As an increasingly connected and decentralized workforce goes online, knowledge work — similarly to software, shopping, and content before it — is shifting online as well. We see people and planet living in harmony without compromising the future of both. Our solution is to give the market exactly what it is required for and more, by pairing Expert Knowledge, economic, environmental, and social vitality throughout the world to achieve more with less.</p>
               <h6><b>Emissions Report</b></h6>
                 <h6><b style={{color:'red'}}>Company Presentation</b></h6>
                 <h6><b style={{color:'red'}}>Financial Results coming soon</b></h6>
                  <h6><b  style={{color:'red'}}>Events </b></h6>
                   <h6><b  style={{color:'red'}}>Meet Beyond Smart Cities</b></h6>
                    <h6><b>Latest Press Releases</b></h6>
                     <h6><b style={{color:'red'}}>Investor Email Alerts</b></h6>
                  <h6><b  style={{color:'red'}}>Quick Links</b></h6>
                   
             </div> 
                  
          </div>
            <div className="row  mb-4">
      <div className="col-md-12"> 
          <div className="contact mb-3">
            <button type="button mb-4"><a href="contact.php">contact</a></button>
            <p>In case, You have any questions about this , please contact us at  <a href="mailto: investors@beyondsmartwork.com"><b> investors@beyondsmartwork.com</b></a></p>
          </div>
          
      </div>
      
  </div>
          
  </div>
  </div>
  
            </div>
        )
    }
}
