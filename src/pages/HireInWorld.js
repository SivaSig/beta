import React, { Component } from 'react'

export default class HireInWorld extends Component {
    render() {
        return (
            <div>
                  <div  className="Nature-banner inner-page">
         
         <div className="row hire-world-banner">
           <div className="col-md-12">
                 <div className="hire-world-banner-content">
                     <h1>Hire the best specialist</h1>
                     <p>Find the right Specialist or agency for your project on the world’s largest hiring platform connecting green businesses and professional f reelancers.</p>
                      <button><a href="">Find Specialist </a></button>
                 </div>
           </div>
       </div>
          <div className="container">
                <div className="row mb-4 mt-4 hire-world-text">
              <div className="col-md-5">
         
                     <h2 className="section-title h2">Type of Work</h2>
        
                    <ul style={{listStyle:'none'}}>
                        <li><a href="Sustainability.php">Sustainability</a> </li>
                       <li><a href="smartcities.php">Smart Cities</a> </li>
                        <li><a href="buildingtech.php">Building Tech</a> </li>
                         <li><a href="energy.php">Energy &amp; CXA</a> </li>
                          <li><a href="climatechange.php">Climate Change</a> </li>
                           <li><a href="firesafety.php">HSE &amp; Fire Safety</a> </li>
                            <li><a href="emergingtech.php">Emerging Tech</a> </li>
                    </ul>
              </div>
              <div className="col-md-7 text-center">
                    <h2 className="section-title p-tb-20 text-center">Explore Specialists on Beyond smart cities</h2>
                    
                    <button><a href="">Search for skills like “LEED” or”GSAS”</a></button>
              </div>
          </div>
          </div>
          
       
       </div>
            </div>
        )
    }
}
