import React, { Component } from 'react'

export default class HireMiddleEast extends Component {
    render() {
        return (
            <div>
                
     <div  className="Nature-banner inner-page">
           <div className="row mb-4">
               <div className="col-md-6 hire">
                         <div className="hire-text-learn">
                              <h1>Hire Specialists in the Middle East</h1>
                         <p>Work with the world’s best specialist on Beyond Smart Cities – the top freelancing website trusted by smart businesses worldwide.</p>
                         <button><a href="">Get Started</a></button>
                       
                         </div>
                        
                  
               </div>
                 <div className="col-md-6 hire-img-layout">
                    
                         <img src="assets/images/middle.jpg" alt="img" />
                    
                   
               </div>
           </div>
           
           <div className="container">
               <div className="row mb-4 middle-layout">
                       <h2 className="section-title p-tb-20 text-center">Connect with a Specialist in the Middle East</h2>
                       
                              <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          
                         
               </div>
                 <div className="row mb-4 middle-layout">
                     
                              <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          <div className="col-md-3">
                             
                               <div className="card">
                                   <div className="profile">
                                      <img src="assets/images/icons/New specialist.png" alt="icon" />
                                    
                                      <div className="profile-content">
                                    <h5>user name</h5>
                                    <h6>Lorem Ipsum is simply </h6>
                                    <h6>dummy text of the printing</h6>
                                    <h6>100% success[16 jobs]</h6>
                                    
                                   </div>
                                   </div>
                                   <div className="d-flex">
                                         <h5>$45/hr</h5> 
                                       <p>United States</p>
                                   </div>
                               
                                 <div className="card-body">
                                <ul>
                                    <li> IOS Developer</li>
                                    <li>Web Developer</li>
                                    <li> Android app Development</li>
                                    <li>IOS Developer</li>
                                    <li> Web Developer</li>
                                </ul>
                                   
                                   
                           
                                   
                          
                                 
 </div>
                               </div>
                             
                         </div>
                          
                         
               </div>
           </div>
           
            </div>
    

            </div>
        )
    }
}
