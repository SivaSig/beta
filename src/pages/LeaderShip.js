import React, { Component } from 'react'

export default class LeaderShip extends Component {
    render() {
        return (
            <div>
                <section>
  <div  className="Nature-banner inner-page">
     <div className="container">

          
                 
   
           
            <div className="row mt-4">
              <div className="col-md-6">
                  <h2 className="about-section-title  text-center">Our Leaders
                  
                  </h2>
                  {/* <!--<p>Facilitating businesses and specialists reach their full potential</p>--> */}
                  
                   <img src="assets/images/Client.png"  alt="" className="img-fluid about-img " />
                    <div className="text-center">
                       <h5>KRISHNAJI PAWAR <br/></h5>
                   <p>CEO & FOUNDER</p>
                   </div>
              </div>
               <div className="col-md-6 p-tb-20">
                   <p><b>KRISHNAJI PAWAR</b></p>
                   <p><b>CEO & FOUNDER</b></p>
                  <p>Krishnaji  Pawar  is  founder  and  CEO  of  Beyond  Smart  Cities.  Before  being  named  CEO  in  January  2020, Krishnaji  held  leadership  roles  at  Beyond  Smart  Cities  in  both  Sustainability  ,Energy  &  Environmental Consultancy.</p>
                 <p>Specialized  in  developing  sustainable  design  strategies  for  Green  Building  Certif ication  Systems  (LEED, GSAS,   etc.),   Energy   &   Water   Conservation,   Commissioning,   Environmental   Impact   Assessment   & Environmental Management Systems. Currently responsible for 3,767 million square feet Green Building /Energy modeling Consulting since January 2007 in UAE, India and Qatar.</p>
                 <p>Having  been  at  Green  Building  Technology  platform  since  2007,  he  is  known  for  bold  leadership  and  his passion  for  the  company’s  mission  to  create  economic  opportunities  so  that  people  live  better  lives  and he’s committed to building a sustainable, high-performing business that delivers on that mission in ever-bigger ways..</p>
                    <p>Before joining Beyond Smart Cities, Krishnaji led sustainability development for Egis International, Qatar & held  corporate  sustainability  development  roles  at  Arab  Engineering  Bureau,  Qatar.  He  began  his  career as  an  Energy  Engineer  at  Pacif ic  Controls  and  Company  after  earning  an  Engineering  Degree     in Mechanical Engineering f rom Visvesvaraya Technological University, Belgaum, India.</p>
              </div>
          </div>
        
      
           </div>
       
         
          
          
          
          
          
          
           </div>
   
</section>

            </div>
        )
    }
}
