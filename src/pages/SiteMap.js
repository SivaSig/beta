import React, { Component } from 'react'

export default class SiteMap extends Component {
    render() {
        return (
            <div>
                 <div  class="Nature-banner inner-page">
      <div class="container">
           <div class="row mb-4">
            <div class="col-md-12">
                <h2 class="section-title p-tb-20 text-center">Site Map</h2>
                <div class="title-divider"></div>
                
                </div>
                
                
                
                
                
                
                
                      </div>
                      <div class="sitemap-box">
                          <div class="row">
                              <div class="col-md-4">
                                   <h5>  <a class="nav-link" href="Sustainability.php">Sustainability</a></h5>
                                    <ul class="navlist-items">
                          <h6>Sustainability Specialist Services</h6>
                          <li><a href="sustainable-building-design.php">Sustainable Building Design</a></li>
                           <li><a href="#">Sustainability Assessments & Reports</a></li>
                             <li><a href="#">Sustainability & ESG Strategy Development</a></li>
                                <li><a href="sustainable-building-design.php">Corporate Social Responsibility (CSR)</a></li>
                            <li><a href="sustainable-building-design.php">Shading Modeling & Analysis</a></li>
                             <li><a href="sustainable-building-design.php">Sustainability Health Check</a></li>
                              <li><a href="sustainable-building-design.php">Sustainable Procurement</a></li>
                          <li><a href="sustainable-building-design.php">Life Cycle Assessment (LCA) </a></li>
                            <li><a href="sustainable-building-design.php">Feasibility & Impact Studies</a></li>
                            <li><a href="sustainable-building-design.php">Stakeholder Engagement</a></li>
                             <li><a href="sustainable-building-design.php">Archaeology & Heritage</a></li>
                             
                              <h6>Social Sustainability</h6> 
                                 <li><a href="sustainable-building-design.php">Social Impact Assessment</a></li>
                          <li><a href="sustainable-building-design.php">Social Impact Assessment</a></li>
                          <h6>Sustainability Strategy & Communications</h6>
                            <li><a href="sustainable-building-design.php">Sustainability Reporting Assurance & Advice</a></li>
                          <li><a href="sustainable-building-design.php">Strategy Development</a></li>
                          <li><a href="sustainable-building-design.php">Environmental Social Governance Due Diligence & Reporting</a></li>
                            <li><a href="sustainable-building-design.php">Environmental Approvals & Management Systems</a></li>
                             <li><a href="sustainable-building-design.php">National Pollutant Inventory Reporting</a></li>
                              <h6>Geosciences</h6>
                        
                             <li><a href="sustainable-building-design.php">Geology</a></li>
                            <li><a href="sustainable-building-design.php">Ecology</a></li>
                             <li><a href="sustainable-building-design.php">Hydrogeology</a></li>
                              <li><a href="sustainable-building-design.php">Contamination Assessment & Remediation</a></li>
                          <li><a href="sustainable-building-design.php">Geotechnical</a></li>  
                           <li><a href="sustainable-building-design.php">Land Quality & Remediation</a></li>
                            <li><a href="sustainable-building-design.php">Contaminated Land Studies Services</a></li>
                             <li><a href="sustainable-building-design.php">Risk Assessment & Toxicology</a></li>
                              <h6>Soil Services</h6>
                          <li><a href="sustainable-building-design.php">Field & Sampling</a></li>
                            <li><a href="sustainable-building-design.php">Geotechnical Services</a></li>
                             <li><a href="sustainable-building-design.php">Laboratory Outsourcing</a></li>
                              <li><a href="sustainable-building-design.php">Environmental Data Management</a></li>
                          <li><a href="sustainable-building-design.php">Interpretation & Modelling</a></li>
                               <li><a href="sustainable-building-design.php">Environmental Assessment & Management</a></li>
                          <li><a href="sustainable-building-design.php">Risk Assessment</a></li>
                          <h6>Water</h6>
                          <li><a href="sustainable-building-design.php">Hydrology & Hydrodynamics</a></li>
                            <li><a href="sustainable-building-design.php">Impact Assessment & Permitting</a></li>
                             <li><a href="sustainable-building-design.php">Integrated Water Management</a></li>
                              <li><a href="sustainable-building-design.php">Wastewater & Stormwater Collection Systems</a></li>
                          <li><a href="sustainable-building-design.php">Wastewater Treatment & Recycling</a></li>
                          <li><a href="sustainable-building-design.php">Water Treatment & Desalination</a></li>
                            <li><a href="sustainable-building-design.php">Water Transmission & Distribution</a></li>
                          <li><a href="sustainable-building-design.php">Water Quality</a></li>
                              <li><a href="sustainable-building-design.php">Water Audits</a></li>
                          <li><a href="sustainable-building-design.php">Marine science</a></li>
                          <h6>Air Quality & Noise</h6>
                           <li><a href="sustainable-building-design.php">Adverse Amenity</a></li>
                            <li><a href="sustainable-building-design.php">Air Quality</a></li>
                             <li><a href="sustainable-building-design.php">Building Acoustics</a></li>
                              <li><a href="sustainable-building-design.php">Expert Witness</a></li>
                          <li><a href="sustainable-building-design.php">CFD</a></li>
                          <li><a href="sustainable-building-design.php">Aviation Noise</a></li>
                            <li><a href="sustainable-building-design.php">Acoustics & Vibration</a></li>
                              <h6>Waste Management</h6>
                             <li><a href="sustainable-building-design.php">Waste Minimization Audits</a></li>
                              <li><a href="sustainable-building-design.php">Organic Waste Management Plan</a></li>
                          <li><a href="sustainable-building-design.php">Supply Chain Management</a></li>
                           <li><a href="sustainable-building-design.php">Solid Waste Management Plan</a></li>
                              <li><a href="sustainable-building-design.php">Construction Waste Management Plan</a></li>
                              <h6>Waste Design & Planning Services</h6>
                          <li><a href="sustainable-building-design.php">Waste Design Services</a></li>
                          <li><a href="sustainable-building-design.php">Technical Waste Services</a></li>
                            <li><a href="sustainable-building-design.php">Waste Transaction Advisory Services</a></li>
                             <li><a href="sustainable-building-design.php">Solid Waste Management</a></li>
                              <li><a href="sustainable-building-design.php">Waste management strategies</a></li>
                          <li><a href="sustainable-building-design.php">Waste to energy solutions</a></li>
                          <h6>Environmental Specialist Services</h6>
                          <li><a href="sustainable-building-design.php">Planning, Policy & Development </a></li>
                          <li><a href="sustainable-building-design.php">Environmental, Social & Governance (ESG) Disclosures</a></li>
                           <li><a href="sustainable-building-design.php">Environmental Impact Assessments(EIA)</a></li>
                              <li><a href="sustainable-building-design.php">Strategic Environmental Assessment(SEA)</a></li>
                          <li><a href="sustainable-building-design.php">Landscape & Visual Impact Assessment</a></li>
                         <li><a href="sustainable-building-design.php">Environmental Monitoring & Modeling</a></li>
                            <li><a href="sustainable-building-design.php">Environmental Site Supervision</a></li>
                                  
                             <li><a href="sustainable-building-design.php">ISO 14001 Environmental management</a></li>
                              <li><a href="sustainable-building-design.php">Construction Environment Management Plan(CEMP)</a></li>
                          <li><a href="sustainable-building-design.php">Standards, KPI & Framework Development</a></li>
                                <h6>Environmental Management, Planning & Approvals</h6>
                                     <li><a href="sustainable-building-design.php">Community Planning</a></li>
                           <li><a href="sustainable-building-design.php">Policy Conception & Implementation</a></li>
                              <li><a href="sustainable-building-design.php">Occupational Hygiene</a></li>
                           <li><a href="sustainable-building-design.php">Environmental Management, Permitting, & Compliance</a></li>
                          <li><a href="sustainable-building-design.php">Landscape Architecture</a></li>
                            <li><a href="sustainable-building-design.php">Eco-Reinforcement</a></li>
                             <li><a href="sustainable-building-design.php">Oil Spill Prevention, Preparedness, & Response Plans</a></li>
                                    
                           
                      </ul>
                      <h5> <a class="nav-link" href="firesafety.php">HSE &amp; Fire
Safety</a></h5>
  <ul class="navlist-items">
                                <h6>Carbon & Energy</h6>
                               
                                <li><a href="sustainable-building-design.php">Greenhouse Gas Inventory, Reporting & Audit</a></li>
                                 <li><a href="sustainable-building-design.php">National Greenhouse & Energy Reporting Advisory & Assurance</a></li>
                                  <li><a href="sustainable-building-design.php">Emissions Abatement Advice & Assurance</a></li>
                                   <li><a href="sustainable-building-design.php">Energy Efficiency & Reduction Strategies</a></li>
                                    <li><a href="sustainable-building-design.php">Carbon Neutrality Advice & Assurance</a></li>
                                     <li><a href="sustainable-building-design.php">Greenhouse Gas Emissions</a></li>
                                 <li><a href="sustainable-building-design.php">Carbon Foot Printing</a></li>
                                  <li><a href="sustainable-building-design.php">GHG Reporting</a></li>
                                   <li><a href="sustainable-building-design.php">GHG Emissions Reduction</a></li>
                                    <li><a href="sustainable-building-design.php">Carbon Offsetting</a></li>
                                     <li><a href="sustainable-building-design.php">CDM Validation</a></li>
                                 <li><a href="sustainable-building-design.php">Verification & Certification for Climate Change Projects</a></li> 
                                  <li><a href="sustainable-building-design.php">Carbon Footprint Certification</a></li>
                                <h6>Climate Change Resilience & Adaptation</h6>
                                <li><a href="sustainable-building-design.php">Climate Due Diligence & Risk Assessment (ISO310000, AS5334)</a></li>
                                    <li><a href="sustainable-building-design.php">Natural Disaster & Adaptative Planning & Resilience</a></li>
                                <li><a href="sustainable-building-design.php">Transition Risk Economics</a></li>
                                 <li><a href="sustainable-building-design.php">Corporate Disclosure Risk Assessments (ASIC, TFCD)</a></li>
                                 <h6>Advisory, ESG & Transactions</h6>
                                  <li><a href="sustainable-building-design.php">Carbon & Energy Management</a></li>
                                   <li><a href="sustainable-building-design.php">ESG Advisory</a></li>
                                   
                                <li><a href="sustainable-building-design.php">Mining Advisory</a></li>
                                 <li><a href="sustainable-building-design.php">Natural Capital & Ecosystem Services</a></li>
                                 <li><a href="sustainable-building-design.php">Oil & Gas Advisory</a></li>
                                  <li><a href="sustainable-building-design.php">Waste & Resource Management</a></li>
                                   <h6>Human Rights & Modern Slavery</h6>
                                   <li><a href="sustainable-building-design.php">Executive Advisory, Awareness Raising & Training</a></li>
                                    <li><a href="sustainable-building-design.php">Legislative Gap Assessment</a></li>
                                     <li><a href="sustainable-building-design.php">Due Diligence, Risk & Impact Assessment</a></li>
                                 <li><a href="sustainable-building-design.php">Design Risk Responses & Remediation</a></li>
                                  <li><a href="sustainable-building-design.php">Mapping & Managing Risks</a></li>
                                   <li><a href="sustainable-building-design.php">Reporting & Compliance</a></li>
                                        
                                
                                  
                                
                          </ul>
                          <h5> <a class="nav-link" href="smartProrating.php">SmartPro <span>Products</span></a></h5>
                                        <ul class="navlist-items">
                          <h6>Building Materials</h6>
                          
                          <li><a href="sustainable-building-design.php">Ready mix concrete</a></li>
                            <li><a href="sustainable-building-design.php">Ceramic Tiles</a></li>
                              <li><a href="sustainable-building-design.php">Tiles-false ceiling</a></li>
                                <li><a href="sustainable-building-design.php">Construction Chemicals</a></li>
                                 <li><a href="sustainable-building-design.php">Construction aggregate</a></li>
                            <li><a href="sustainable-building-design.php">Construction blocks</a></li>
                              <li><a href="sustainable-building-design.php">Construction blocks</a></li>
                                <li><a href="sustainable-building-design.php">Green Cement</a></li>
                                 <li><a href="sustainable-building-design.php">Water Harvesting System</a></li>
                            <li><a href="sustainable-building-design.php">Gypsum boards</a></li>
                              <li><a href="sustainable-building-design.php">Panels and laminates</a></li>
                                <li><a href="sustainable-building-design.php">Blinds and curtains</a></li>
                                  <li><a href="sustainable-building-design.php">Bricks and blocks</a></li>
                            <li><a href="sustainable-building-design.php">Carpets</a></li>
                              <li><a href="sustainable-building-design.php">Doors and windows</a></li>
                                <li><a href="sustainable-building-design.php">Rapidly renewable materials</a></li>
                                 <li><a href="sustainable-building-design.php">Sustainable Flooring</a></li>
                                  <h6>Interior Products</h6>
                            <li><a href="sustainable-building-design.php">Furniture</a></li>
                              <li><a href="sustainable-building-design.php">Art</a></li>
                                <li><a href="sustainable-building-design.php">Wall Covering</a></li>
                                 <li><a href="sustainable-building-design.php">Kitchens</a></li>
                            <li><a href="sustainable-building-design.php">Gardening</a></li>
                              <li><a href="sustainable-building-design.php">Outdoor</a></li>
                                <li><a href="sustainable-building-design.php">Kitchen & Dining Fixtures</a></li>
                                <h6>Building Envelope</h6>
                                 <li><a href="sustainable-building-design.php">High Performance Glass</a></li>
                            <li><a href="sustainable-building-design.php">High albedo roof paints</a></li>
                              <li><a href="sustainable-building-design.php">Insulation</a></li>
                                <li><a href="sustainable-building-design.php">Panels and laminates</a></li>
                                 <li><a href="sustainable-building-design.php">Green roof</a></li>
                            <li><a href="sustainable-building-design.php">UPVC windows and doors</a></li>
                              <h6>Heating, ventilation, and air conditioning (HVAC)</h6>
                              <li><a href="sustainable-building-design.php">Air conditioning technology</a></li>
                                <li><a href="sustainable-building-design.php">Air curtains</a></li>
                                 <li><a href="sustainable-building-design.php">Air handling units and fan coil units</a></li>
                            <li><a href="sustainable-building-design.php">Bureau of energy efficiency rated</a></li>
                              <li><a href="sustainable-building-design.php">Unitary air handling units</a></li>
                                <li><a href="sustainable-building-design.php">Chillers</a></li>
                                 <li><a href="sustainable-building-design.php">Cooling towers</a></li>
                            <li><a href="sustainable-building-design.php">Ducts and piping systems</a></li>
                              <li><a href="sustainable-building-design.php">Energy recovery wheels</a></li>
                                <li><a href="sustainable-building-design.php">Geothermal cooling</a></li>
                                 <li><a href="sustainable-building-design.php">Reading cooling technology</a></li>
                            <li><a href="sustainable-building-design.php">Chilled beams</a></li>
                              <li><a href="sustainable-building-design.php">Variable air volume</a></li>
                                <li><a href="sustainable-building-design.php">Variable frequency drive(VFD) System</a></li>
                                 <li><a href="sustainable-building-design.php">Variable refrigerant volume/flow(VRV/VRF) Technology</a></li>
                                 <h6>Lighting</h6>
                            <li><a href="sustainable-building-design.php">Electronic ballasts</a></li>
                              <li><a href="sustainable-building-design.php">Lighting fixtures</a></li>
                                <li><a href="sustainable-building-design.php">Lighting management system</a></li>
                                 <li><a href="sustainable-building-design.php">Lighting sensers</a></li>
                            <li><a href="sustainable-building-design.php">Lighting pipes</a></li>
                              <li><a href="sustainable-building-design.php">LED</a></li>
                                <li><a href="sustainable-building-design.php">Tubular Skylights</a></li>
                                 <li><a href="sustainable-building-design.php">Test Kits & Energy Monitors</a></li>
                                 <h6>Renewable energy</h6>
                            <li><a href="sustainable-building-design.php">Bio gas/bio mass plants</a></li>
                              <li><a href="sustainable-building-design.php">Mini hydro plants</a></li>
                                <li><a href="sustainable-building-design.php">Solar air conditioning</a></li>
                                 <li><a href="sustainable-building-design.php">Solar photo voltaics</a></li>
                            <li><a href="sustainable-building-design.php">Solar water heating</a></li>
                              <li><a href="sustainable-building-design.php">Solar -wind hybrid systems</a></li>
                                <li><a href="sustainable-building-design.php">Wind energy</a></li>
                                 <h6>Energy Equipment</h6>
                           <li><a href="sustainable-building-design.php">Building management system</a></li>
                            <li><a href="sustainable-building-design.php">Conveyers ,crushers and screeners</a></li>
                              <li><a href="sustainable-building-design.php">Electric vehicle</a></li>
                                <li><a href="sustainable-building-design.php">Electric bike</a></li>
                                 <li><a href="sustainable-building-design.php">Elevators and escalators</a></li>
                            <li><a href="sustainable-building-design.php">Generators</a></li>
                              <li><a href="sustainable-building-design.php">Pumps and motors</a></li>
                                <li><a href="sustainable-building-design.php">Power distribution</a></li>
                        <h6>Water Efficiency</h6>
                                 <li><a href="sustainable-building-design.php">Water efficient fixtures</a></li>
                            <li><a href="sustainable-building-design.php">Pressure reducing valves</a></li>
                              <li><a href="sustainable-building-design.php">Water less urinals</a></li>
                                <li><a href="sustainable-building-design.php">Water treatment technology and management</a></li>
                                 <li><a href="sustainable-building-design.php">Water Purification</a></li>
                            <li><a href="sustainable-building-design.php">Waste water management</a></li>
                              <li><a href="sustainable-building-design.php">Smart  Water Metering</a></li>
                              <h6>Architectural Products</h6>
                                <li><a href="sustainable-building-design.php">Perforated Polymer Concrete Panels</a></li>
                                 <li><a href="sustainable-building-design.php">Metal Fabrics</a></li>
                            <li><a href="sustainable-building-design.php">Aluminium Windows</a></li>
                              <li><a href="sustainable-building-design.php">Hydraulic Tiles</a></li>
                                <li><a href="sustainable-building-design.php">Fibre Cement Façade Material</a></li>
                                 <li><a href="sustainable-building-design.php">Perforated Metal Panels for Facades</a></li>
                            <li><a href="sustainable-building-design.php">Custom Made Doors</a></li>
                            <h6>Indoor Environment Quality</h6>
                              <li><a href="sustainable-building-design.php">Air handling unit filters</a></li>
                                <li><a href="sustainable-building-design.php">Co2 sensers</a></li>
                                 <li><a href="sustainable-building-design.php">Eco-friendly housekeeping chemicals</a></li>
                            <li><a href="sustainable-building-design.php">Entry way system</a></li>
                              <li><a href="sustainable-building-design.php">Indoor Air Quality Solutions</a></li>
                                <li><a href="sustainable-building-design.php">Low emitting materials</a></li>
                                 <li><a href="sustainable-building-design.php">Adhesives and sealants</a></li>
                            <li><a href="sustainable-building-design.php">Carpets</a></li>
                              <li><a href="sustainable-building-design.php">Paints & Primers</a></li>
                                <li><a href="sustainable-building-design.php">Strippers & Thinners</a></li>
                                 <li><a href="sustainable-building-design.php">Plasters & Pigments</a></li>
                            <li><a href="sustainable-building-design.php">Caulks, Sealants & Adhesives</a></li>
                              <li><a href="sustainable-building-design.php">Hardwood Floor Finish</a></li>
                                <li><a href="sustainable-building-design.php">Wood Stains & Sealers</a></li>
                                 <li><a href="sustainable-building-design.php">Grout Sealers</a></li>
                            <li><a href="sustainable-building-design.php">Concrete & Masonry Sealers</a></li>
                              <li><a href="sustainable-building-design.php">Polyurethane Sealers</a></li>
                              <h6>Testing facilities</h6>
                                <li><a href="sustainable-building-design.php">FSC Accreditation</a></li>
                                 <li><a href="sustainable-building-design.php">Indoor Air Quality Testing</a></li>
                              <li><a href="sustainable-building-design.php">SRI Value</a></li>
                                <li><a href="sustainable-building-design.php">U Value</a></li>
                                 <li><a href="sustainable-building-design.php">VOC testing</a></li>
                                 <h6>Innovative Product & Technology</h6>
                            <li><a href="sustainable-building-design.php">Aerogel Insulation</a></li>
                              <li><a href="sustainable-building-design.php">Transparent Aluminium</a></li>
                                <li><a href="sustainable-building-design.php">Self-healing concrete</a></li>
                                 <li><a href="sustainable-building-design.php">Augmented reality and virtual reality</a></li>
                              <li><a href="sustainable-building-design.php">Building Information Modelling (BIM)</a></li>
                                <li><a href="sustainable-building-design.php">Robotics</a></li>
                                 <li><a href="sustainable-building-design.php">Cloud and Mobile Technology</a></li>
                            <li><a href="sustainable-building-design.php">Drones</a></li>
                              <li><a href="sustainable-building-design.php">IoT for construction</a></li>
                                <li><a href="sustainable-building-design.php">Smart Buildings</a></li>
                                <h6>Health Care & Wellness</h6>
                                 <li><a href="sustainable-building-design.php">Weight management</a></li>
                              <li><a href="sustainable-building-design.php">Dynamic meditation</a></li>
                                <li><a href="sustainable-building-design.php">Greening</a></li>
                                 <li><a href="sustainable-building-design.php">Yoga</a></li>
                            <li><a href="sustainable-building-design.php">Organic Products</a></li>
                              <li><a href="sustainable-building-design.php">Fitness</a></li>
                                <li><a href="sustainable-building-design.php">Agni Hotra</a></li>
                                 <li><a href="sustainable-building-design.php">Healthy Cleaners</a></li>
                                <li><a href="sustainable-building-design.php">Household</a></li>
                                 <li><a href="sustainable-building-design.php">Vedic products</a></li>
                                 <h6>Service Providers</h6>
                              <li><a href="sustainable-building-design.php">Green building Consultants</a></li>
                                <li><a href="sustainable-building-design.php">Energy modelling Consultants</a></li>
                                 <li><a href="sustainable-building-design.php">Certified Energy Auditors </a></li>
                            <li><a href="sustainable-building-design.php">Commissioning Agents</a></li>
                              <li><a href="sustainable-building-design.php">ISO 14001:2015 Auditing</a></li>
                               
          </ul>
                              </div>
                               <div class="col-md-4">
                                  <h5>
                                        <a class="nav-link" href="smartcities.php">Smart Citiesy</a>
                                  </h5>
                                  <ul class="navlist-items">
                          <h6>Beyond Smart Cities</h6>
                           <li><a href="sustainable-building-design.php">Smart Campus</a></li>
                            <li><a href="sustainable-building-design.php">Smart Parking</a></li>
                             <li><a href="sustainable-building-design.php">Smart Waste Management </a></li>
                              <li><a href="sustainable-building-design.php">Smart Building Management Systems</a></li>
                               <li><a href="sustainable-building-design.php">Smart Water</a></li>
                                <li><a href="sustainable-building-design.php">Smart Energy Management </a></li>
                            <li><a href="sustainable-building-design.php">Smart Environment Management Solution</a></li>
                            <h6>Infrastructure</h6>
                             <li><a href="sustainable-building-design.php">Airport Planning</a></li>
                              <li><a href="sustainable-building-design.php">Bridge Engineering</a></li>
                               <li><a href="sustainable-building-design.php">Civil Engineering</a></li>
                                <li><a href="sustainable-building-design.php">Infrastructure Design</a></li>
                            <li><a href="sustainable-building-design.php">Maritime Engineering</a></li>
                             <li><a href="sustainable-building-design.php">Rail Engineering</a></li>
                              <li><a href="sustainable-building-design.php">Tunnel Design</a></li>
                           <li><a href="sustainable-building-design.php">Infrastructure Master Planning</a></li>
                                <li><a href="sustainable-building-design.php">Water & Waste Water</a></li>
                            <li><a href="sustainable-building-design.php">Public Utilities</a></li>
                             <li><a href="sustainable-building-design.php">Campus Central Plant Designs</a></li>
                             <h6>Planning</h6>
                        <li><a href="sustainable-building-design.php">Economic Planning</a></li>
                               <li><a href="sustainable-building-design.php">Environmental Consulting</a></li>
                        <li><a href="sustainable-building-design.php">Flood Risk Management</a></li>
                                <li><a href="sustainable-building-design.php">Landscape Architecture</a></li>
                            <li><a href="sustainable-building-design.php">Master Planning</a></li>
                             <li><a href="sustainable-building-design.php">Planning Policy Advice</a></li>
                              <li><a href="sustainable-building-design.php">Resilience Security & Ris</a></li>
                               <li><a href="sustainable-building-design.php">Statutory, Strategic & Integrated Approvals</a></li>
                                <li><a href="sustainable-building-design.php">Property Services</a></li>
                            <li><a href="sustainable-building-design.php">GIS/Spatial Data Planning Services</a></li>
                             <li><a href="sustainable-building-design.php">Legislative Compliance</a></li>
                              
                              
                                <h6>Transportation</h6>
                         <li><a href="sustainable-building-design.php">Active transportation, bicycle and pedestrian advice</a></li>
                             <li><a href="sustainable-building-design.php">Demand management</a></li>
                              <li><a href="sustainable-building-design.php">Freight planning and advice</a></li>
                               <li><a href="sustainable-building-design.php">Integrated land use, transport advice and transport studies</a></li>
                                 <li><a href="sustainable-building-design.php">Parking strategies, analysis and advice</a></li>
                             <li><a href="sustainable-building-design.php">Pedestrian simulation and traffic and transport modelling</a></li>
                              <li><a href="sustainable-building-design.php">Public transport infrastructure and service advice</a></li>
                               <li><a href="sustainable-building-design.php">Road safety audits and analysis</a></li>
                               <li><a href="sustainable-building-design.php">Strategic route planning and feasibility studies</a></li>
                                 <li><a href="sustainable-building-design.php">Traffic engineering assessments and advice</a></li>
                                 <h6>Road Systems</h6>
                             <li><a href="sustainable-building-design.php">Planning</a></li>
                              <li><a href="sustainable-building-design.php">Feasibilit</a></li>
                               <li><a href="sustainable-building-design.php">Investigation</a></li>
                               <li><a href="sustainable-building-design.php">Design and engineerin</a></li>
                                 <li><a href="sustainable-building-design.php">Construction</a></li>
                             <li><a href="sustainable-building-design.php">Roads and Bridge Design</a></li>
                             <h6>Railways</h6>
                              <li><a href="sustainable-building-design.php">Intelligent Transport Systems</a></li>
                               <li><a href="sustainable-building-design.php">Traffic Impact Assessments</a></li>
                                  <li><a href="sustainable-building-design.php">Transport Strategies</a></li>
                               <li><a href="sustainable-building-design.php">Travel Behaviour Change</a></li>
                                  <li><a href="sustainable-building-design.php">Urban Revitalization Transport Advice</a></li>
                          </ul>
                           <h5> <a class="nav-link" href="buildingtech.php">Building Tech</a></h5>
                                    <ul class="navlist-items">
                           <h6>Digital Design</h6>
          <li><a href="sustainable-building-design.php">2D and 3D CAD-Based Design & Documentation</a></li>
                                  <li><a href="sustainable-building-design.php">Building Information Modeling (BIM)</a></li>
                               <li><a href="sustainable-building-design.php">Clash & Interference Detection</a></li>
        
        
         <li><a href="sustainable-building-design.php">Design Analysis & Visualisation</a></li>
                               <li><a href="sustainable-building-design.php">Digital Asset Management</a></li>
                        <li><a href="sustainable-building-design.php">Interior Architecture</a></li>
                                <li><a href="sustainable-building-design.php">Landscape</a></li>
                                <h6>Building Sciences & Physics</h6>
                            <li><a href="sustainable-building-design.php">Accessible Environments</a></li>
                             <li><a href="sustainable-building-design.php">Construction Review Services</a></li>
                              <li><a href="sustainable-building-design.php">Tendering & Contract Administration Services</a></li>
                               <li><a href="sustainable-building-design.php">Architecture</a></li>
                                <li><a href="sustainable-building-design.php">Building Design</a></li>
                            <li><a href="sustainable-building-design.php">Building Envelope & Facade Design</a></li>
                             <li><a href="sustainable-building-design.php">Building physics</a></li>
                              <li><a href="sustainable-building-design.php">Building Retrofit</a></li>
                               <li><a href="sustainable-building-design.php">Building Services Engineering</a></li>
                                <li><a href="sustainable-building-design.php">Commissioning & Building Performance Evaluation</a></li>
                                  <li><a href="sustainable-building-design.php">Electrical Engineering</a></li>
                               <li><a href="sustainable-building-design.php">Facilities Management</a></li>
        
        
         <li><a href="sustainable-building-design.php">Mechanical Engineering</a></li>
                               <li><a href="sustainable-building-design.php">Vertical Transport Design</a></li>
                        <li><a href="sustainable-building-design.php">Wind Engineering</a></li>
                        <h6>Engineering</h6>
                                <li><a href="sustainable-building-design.php">Civil & Structural Engineering</a></li>
                            <li><a href="sustainable-building-design.php">Construction Services</a></li>
                             <li><a href="sustainable-building-design.php">Geotechnical Engineering</a></li>
                              <li><a href="sustainable-building-design.php">Land Surveying</a></li>
                               <li><a href="sustainable-building-design.php">Mine Waste Engineering</a></li>
                                <li><a href="sustainable-building-design.php">Process Engineering</a></li>
                            <li><a href="sustainable-building-design.php">Transportation Engineering</a></li>
                             <li><a href="sustainable-building-design.php">Water & Wastewater Engineering</a></li>
                              <li><a href="sustainable-building-design.php">Water Resource Engineering</a></li>
                               <li><a href="sustainable-building-design.php">Design Review & Technical Services</a></li>
                                           
          <li><a href="sustainable-building-design.php">Lighting Design</a></li>
          <h6>Green Certification Auditing & Management</h6>
              <li><a href="sustainable-building-design.php">LEED</a></li>
                               <li><a href="sustainable-building-design.php">IGBC LEED</a></li>
        
        
         <li><a href="sustainable-building-design.php">GRIHA</a></li>
                               <li><a href="sustainable-building-design.php">WELL</a></li>
                        <li><a href="sustainable-building-design.php">BREEAM</a></li>
                                <li><a href="sustainable-building-design.php">GSAS</a></li>
                            <li><a href="sustainable-building-design.php">Dubai Green Building Regulations (Al Sa'fat)</a></li>
                            <li><a href="sustainable-building-design.php">ENVISION</a></li>
                               <li><a href="sustainable-building-design.php">Estidama</a></li>
        
        
         <li><a href="sustainable-building-design.php">GRESB</a></li>
         <h6>Automation</h6>
                               <li><a href="sustainable-building-design.php">ICT - Information & Communications Technology</a></li>
                        <li><a href="sustainable-building-design.php">Controls on Cloud</a></li>
                                <li><a href="sustainable-building-design.php">Software as a Service</a></li>
                            <li><a href="sustainable-building-design.php">Managed Services</a></li>
                             <li><a href="sustainable-building-design.php">Green Data Centers</a></li>
                              <li><a href="sustainable-building-design.php">Machine to Machine (M2M)</a></li>
                               <li><a href="sustainable-building-design.php">Airport Automation</a></li>
                                <li><a href="sustainable-building-design.php">Hotel Automation</a></li>
                            <li><a href="sustainable-building-design.php">Integrated Building Automation</a></li>
                             <li><a href="sustainable-building-design.php">e-Enabled Green Homes</a></li>
                              <li><a href="sustainable-building-design.php">Vehicle Tracking System</a></li>
                               <li><a href="sustainable-building-design.php">Supervisory Control & Data Acquisition (SCADA) Network Architecture Design</a></li>
                                <li><a href="sustainable-building-design.php">Telemetry & Communications Systems Design</a></li>
                                <li><a href="sustainable-building-design.php">Safety Integrity Level (SIL) Assignment & Assessments</a></li>
                            <li><a href="sustainable-building-design.php">Safety Instrumented Systems (SIS)</a></li>
                             <li><a href="sustainable-building-design.php">Hazardous Area Management System (HAMS)</a></li>
                             <h6>Project Construction Management</h6>
                              <li><a href="sustainable-building-design.php">Program Management</a></li>
                               <li><a href="sustainable-building-design.php">Project Management</a></li>
                                <li><a href="sustainable-building-design.php">Construction Supervision</a></li>
                            <li><a href="sustainable-building-design.php">Design Review & Technical Services</a></li>
                             <li><a href="sustainable-building-design.php">Health, Safety & Environmenta</a></li>
                              <li><a href="sustainable-building-design.php">Schedule Management</a></li>
                               <li><a href="sustainable-building-design.php">Cost Management</a></li>
                                <li><a href="sustainable-building-design.php">Cost Management</a></li>
                            <li><a href="sustainable-building-design.php">Cost Management</a></li>
                             <li><a href="sustainable-building-design.php">Value Engineering</a></li>
                              <li><a href="sustainable-building-design.php">Claims Management</a></li>
     
        </ul>
                       <h5>  <a class="nav-link" href="knowledge-lab.php">Knowledge lab</a></h5>
                       
                      <ul class="navlist-items">
                          <h6>Sustainability and Environmen</h6>
                         <li><a href="sustainable-building-design.php">Sustainability Management</a></li>
                                   <li><a href="sustainable-building-design.php">Construction Environmental Management Pla</a></li>
                                    <li><a href="sustainable-building-design.php">Environmental Impact Assessment</a></li>
                                    <h6>Green Building Technology</h6>
                                     <li><a href="sustainable-building-design.php">Beyond Smart Cities</a></li>
                                    <li><a href="sustainable-building-design.php">Beyond Green Building Technology</a></li>
                                     
          <h6>Building Commissioning</h6>
            <li><a href="sustainable-building-design.php">Green Building Commissioning</a></li>
            <li><a href="sustainable-building-design.php">Advanced Green Building Commissioning</a></li>
                <h6>Energy Technology</h6>
                                    <li><a href="sustainable-building-design.php">Advanced Building Energy Modeling</a></li>
                                    <h6>Emerging Design & Technology</h6>
            <li><a href="sustainable-building-design.php">Blockchain Technology</a></li>
            <li><a href="sustainable-building-design.php">Digital Transformation</a></li>
            <li><a href="sustainable-building-design.php">Artificial Intelligence</a></li>
            <li><a href="sustainable-building-design.php">Internet of Things</a></li>
            <h6>Lifestyle</h6>
         <li><a href="sustainable-building-design.php">Dynamic Meditation</a></li>
         
         
         
               </ul>
                              </div>
                               <div class="col-md-4">
                                 <h5><a class="nav-link" href="energy.php">Energy &amp; CXA</a></h5>
                                  <ul class="navlist-items">
                                <h6>Energy Simulation</h6>
                               
                                <li><a href="sustainable-building-design.php">Energy Modeling & Analysis</a></li>
                                 <li><a href="sustainable-building-design.php">Daylight Modeling & Analysis</a></li>
                                  <li><a href="sustainable-building-design.php">Measurement & Verification</a></li>
                                   <li><a href="sustainable-building-design.php">System Design & Evaluation</a></li>
                                   <h6>Energy Efficiency</h6>
                                    <li><a href="sustainable-building-design.php">Energy Audits & Optimization Services</a></li>
                                     <li><a href="sustainable-building-design.php">Green Building Schemes</a></li>
                                 <li><a href="sustainable-building-design.php">ISO 50001 Energy Management Systems</a></li>
                                  <li><a href="sustainable-building-design.php">Energy Savings Measurement & Verification (M&V)</a></li>
                                   <li><a href="sustainable-building-design.php">White Certificates & Energy Savings Schemes</a></li>
                                    <li><a href="sustainable-building-design.php">Energy Performance Assessment & Monitoring</a></li>
                                   <h6>Lighting Design & Analysis</h6>
                                 <li><a href="sustainable-building-design.php">Interior Lighting Modeling & Analysis</a></li>
                                 <li><a href="sustainable-building-design.php">Exterior Lighting Modeling & Analysis</a></li>
                                  <li><a href="sustainable-building-design.php">Day Lighting Modeling & Analysis</a></li>
                                   <li><a href="sustainable-building-design.php">Compliance Lighting Report as per ASHARE 90.1</a></li>
                                    <li><a href="sustainable-building-design.php">Energy Evaluation Report</a></li>
                                <li><a href="sustainable-building-design.php">Lighting Analysis Video Presentation</a></li>
                                 <li><a href="sustainable-building-design.php">Lighting Analysis CAD Layout</a></li>
                                  <li><a href="sustainable-building-design.php">Concept & Design of Lighting Control System</a></li>
                                  <h6>Energy Managemen</h6>
                                   <li><a href="sustainable-building-design.php">Energy Auditing & Management</a></li>
                                    <li><a href="sustainable-building-design.php">Electricity Market Analysis</a></li>
                                     <li><a href="sustainable-building-design.php">Power Market Design & Analysis</a></li>
                                 <li><a href="sustainable-building-design.php">Facilitation of Renewable Energy Integration</a></li>
                                  <li><a href="sustainable-building-design.php">Strategic Planning & Investment Decisions</a></li>
                                   <li><a href="sustainable-building-design.php">Quantitative Electricity Market Segment Analysis</a></li>
                                   <h6>Energy Audit Services</h6>
                                    <li><a href="sustainable-building-design.php">Investment Grade Energy Audits (IGEA)</a></li>
                                     <li><a href="sustainable-building-design.php">Detailed Energy Audits (DEA)</a></li>
                                 <li><a href="sustainable-building-design.php">Mandatory Energy Audits (MEA)</a></li>
                                  <li><a href="sustainable-building-design.php">Monitoring & Verification (M&V) Audits PAT M&V</a></li>
                                   <li><a href="sustainable-building-design.php">Energy   Audit   for   Micro,   Small   and   Medium   Enterprise(MSMEs)</a></li>
                                    <li><a href="sustainable-building-design.php">ASHRAE Level 1 & 2 Energy Audits</a></li>
                                <h6>Renewable Energy</h6>
                               
                                <li><a href="sustainable-building-design.php">Due Diligence for Renewable Energy Projects</a></li>
                                 <li><a href="sustainable-building-design.php">In-Service Inspections</a></li>
                                  <li><a href="sustainable-building-design.php">Infrared Inspections</a></li>
                                   <li><a href="sustainable-building-design.php">Biomass Testing & Certification</a></li>
                                    <li><a href="sustainable-building-design.php">Self-Consumption Feasibility Services (Photovoltaic)</a></li>
                                    <h6>Building Commissioning</h6>
                                     <li><a href="sustainable-building-design.php">Owners Project Requirement(OPR)</a></li>
                                 <li><a href="sustainable-building-design.php">Basis of Design(BOD)</a></li>
                                  <li><a href="sustainable-building-design.php">Commissioning Plan</a></li>
                                   <li><a href="sustainable-building-design.php">LEED Fundamental Commissioning</a></li>
                                    <li><a href="sustainable-building-design.php">LEED Enhanced Commissioning</a></li>
                                     <li><a href="sustainable-building-design.php">Recommissioning</a></li>
                                 <li><a href="sustainable-building-design.php">Retro-commissioning</a></li>
                                  <li><a href="sustainable-building-design.php">Envelope Commissioning</a></li>
                                   <li><a href="sustainable-building-design.php">HVAC Commissioning</a></li>
                                    <li><a href="sustainable-building-design.php">O&M Training</a></li>
                                    <h6>Testing, adjusting, and balancing</h6>
                                      <li><a href="sustainable-building-design.php">Existing Facility Model Calibration</a></li>
                                  <li><a href="sustainable-building-design.php">Quality Assurance & Quality Control</a></li>
                                   <li><a href="sustainable-building-design.php">Process Safety Loss Prevention</a></li>
                                    <li><a href="sustainable-building-design.php">Functional Safety Services</a></li>
                                      <li><a href="sustainable-building-design.php">Special Inspection Smoke Control</a></li>
                                  <li><a href="sustainable-building-design.php">Building Envelope & Acoustics Testing</a></li>
                                   <li><a href="sustainable-building-design.php">Blower Door Testing</a></li>
                                    <li><a href="sustainable-building-design.php">Equipment Testing & Long-term Monitoring</a></li>
                                      <li><a href="sustainable-building-design.php">Post-installation Inspection & Commissioning Services</a></li>
                                
                          </ul>
                          <h5> <a class="nav-link" href="climatechange.php">GHG &amp; Climate Change</a></h5>
                                 <ul class="navlist-items">
                                <h6>Environmental Specialist Services</h6>
                               
                                <li><a href="sustainable-building-design.php">Planning, Policy & Development</a></li>
                                 <li><a href="sustainable-building-design.php">Environmental, Social and Governance (ESG) Disclosures</a></li>
                                  <li><a href="sustainable-building-design.php">Environmental Impact Assessments(EIA)</a></li>
                                   <li><a href="sustainable-building-design.php">Strategic Environmental Assessment(SEA)</a></li>
                                    <li><a href="sustainable-building-design.php">Landscape & visual impact assessment</a></li>
                                     <li><a href="sustainable-building-design.php">Landscape & visual impact assessment</a></li>
                                 <li><a href="sustainable-building-design.php">Environmental Due Diligence Assessments</a></li>
                                  <li><a href="sustainable-building-design.php">Environmental Monitoring and Modeling</a></li>
                                   <li><a href="sustainable-building-design.php">Environmental Site Supervision </a></li>
                                    <li><a href="sustainable-building-design.php">ISO 14001 Environmental management</a></li>
                                     <li><a href="sustainable-building-design.php">CEMP</a></li>
                                 <li><a href="sustainable-building-design.php">Occupational Hygiene</a></li>
                                  <li><a href="sustainable-building-design.php">Standards, KPI & Framework Development</a></li>
                                    <h6>Environmental Health & Safety Services</h6>
                               <li><a href="sustainable-building-design.php">Environment, Health and Safety </a></li>
                                    <li><a href="sustainable-building-design.php">Global Health and Environmental Regulatory</a></li>
                                <li><a href="sustainable-building-design.php">EHS Management and Compliance Solutions</a></li>
                                 <li><a href="sustainable-building-design.php">Dismantling and Treatment Instructions Services for Recyclers</a></li>
                                  <li><a href="sustainable-building-design.php">Bill of Material (BoM) Assessment</a></li>
                                   <li><a href="sustainable-building-design.php">Process Safety Management (PSM) Consulting</a></li>
                                    <li><a href="sustainable-building-design.php">Scientific Assessments & Reviews</a></li>
                                     <li><a href="sustainable-building-design.php">Hazardous Material Assessments & Management Programs</a></li>
                                 <li><a href="sustainable-building-design.php">Community Education & Outreach Program</a></li>
                                  <li><a href="sustainable-building-design.php">ISO 45001 Occupational Health & Safety</a></li>
                                    <h6>Fire and Life Safety</h6>
                                   <li><a href="sustainable-building-design.php">Fire Audit</a></li>
                                    <li><a href="sustainable-building-design.php">Fire Risk Assessment</a></li>
                                    
                                 <li><a href="sustainable-building-design.php">Fire Pre-plan</a></li>
                                 <li><a href="sustainable-building-design.php">Safety Audit</a></li>
                                  <li><a href="sustainable-building-design.php">Hazard Identification & Risk Assessment</a></li>
                                   <li><a href="sustainable-building-design.php">National Fire Protection Association (NFPA), Insurer & Fire Test Standards</a></li>
                                    <li><a href="sustainable-building-design.php">Fire & Building Authorities Permitting & Regulatory Approvals Assistance</a></li>
                                <li><a href="sustainable-building-design.php">Fire Engineering Design Briefs & Reports</a></li>
                                 <li><a href="sustainable-building-design.php">Performance-Based Design & Alternative Solutions</a></li>
                                  <li><a href="sustainable-building-design.php">Fire & Egress Computer Modeling</a></li>
                                   <li><a href="sustainable-building-design.php">Fire & Life Safety Master Plans</a></li>
                                    <li><a href="sustainable-building-design.php">Due Diligence & Loss Prevention Assessments</a></li>
                                     <li><a href="sustainable-building-design.php">Dangerous Goods & Hazardous Materials Analysis</a></li>
                                 <li><a href="sustainable-building-design.php">Smoke Management Consulting</a></li>
                                  <li><a href="sustainable-building-design.php">Explosion Prevention, Mitigation & Venting</a></li>
                                   <li><a href="sustainable-building-design.php">Emergency Preparedness</a></li>
                                    
                                
                          </ul>
                          <h5> <a class="nav-link" href="emergingtech.php">Emerging Tech</a></h5>
                          
                      <ul class="navlist-items">
                              
                               
                                <li><a href="sustainable-building-design.php">Artificial Intelligence (AI) </a></li>
                                 <li><a href="sustainable-building-design.php">Machine Learning (ML)</a></li>
                                  <li><a href="sustainable-building-design.php">Augmented Reality (AR)</a></li>
                                   <li><a href="sustainable-building-design.php">Virtual Reality (VR)</a></li>
                                    <li><a href="sustainable-building-design.php">Big Data & Smart Cities</a></li>
                                      <li><a href="sustainable-building-design.php">Traffic Management</a></li>
                                       <li><a href="sustainable-building-design.php">Maintenance Management</a></li>
                                  <li><a href="sustainable-building-design.php">Smart Grids</a></li>
                                   <li><a href="sustainable-building-design.php">Block Chain Technology</a></li>
                                    <li><a href="sustainable-building-design.php">Building Information Modeling (BIM)</a></li>
                                     <li><a href="sustainable-building-design.php">City Information Modeling (CIM)</a></li>
                                 <li><a href="sustainable-building-design.php">Digital Twin</a></li>
                                 <li><a href="sustainable-building-design.php">GeoSpatial</a></li>
                                   <li><a href="sustainable-building-design.php">Internet of Things (IoT)</a></li>
                                    <li><a href="sustainable-building-design.php">Mobile Devices & APPs</a></li>
                                     <li><a href="sustainable-building-design.php">Sensors &  the Sensor Web</a></li>
                                    <li><a href="sustainable-building-design.php">Smart Investments</a></li>
                                     <li><a href="sustainable-building-design.php">Smart Capital</a></li>
                                   
                               
                                 
                                
                          </ul>
                              </div>
                          </div>
                          </div>
    </div>
    
            </div>
            </div>
        )
    }
}
