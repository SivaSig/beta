import React, { Component } from 'react'

export default class Careers extends Component {
    render() {
        return (
            <div>
                <div  className="Nature-banner inner-page">
         
         <div className="row careers">
         <div className="col-md-12">
               <div className="careers-banner">
                   <h1>Independent talent at the heart of every business</h1>
                   <p>Be part of the team that's bringing this vision to life</p>
                   <button><a href="">Find Your  Team</a></button>
               </div>
         </div>
     </div>
     
     
     
     
  <div className="container mobile-careers">
       <div className="row mb-4">
        <div className="col-md-12">
            <h2 className="section-title p-tb-20 text-center">Working at Beyond Smart Cities</h2>
             <p>At Beyond Smart Cities, we're solving the world's most important problems with talented individuals who share our passion to change the world. Our culture is fast-paced, energetic and innovative. It is our mission to grow and cultivate the world’s largest digital marketplace, a place where people can f ind and purchase any professional service they need and build any business they dream</p>
            
          
     </div>
      </div>
      <div className="row  mb-4">
          <div className="col-md-6">
              <div className="careers-join">
                    <h4 className="  text-center">JOIN US.LETS BUILD A SUSTAINABLE PLANET</h4>
              <p>B e y o n d   S m a r t   C i t i e s   i s   t h e   I n d i a ’ s   w o r k marketplace   that   connects   businesses   with independent  talent.  We  serve  everyone  f rom  one-person  startups  to  MNC  with  a  powerful,  trust-driven  platform  that  enables  companies  ,product manufactures  ,property  owners  and  f reelancers  to work   together   in   new   ways   that   unlock   their potential.    We  are  making  the  future  cause  of  our present. And you could be part of it.</p>
              <div className="text-center">
              <button><a href="">Follow us @beyondsmartcities</a></button>
              <br/>
              <br/>
              <button><a href="">Follow us @beyondsmartcities</a></button>
             </div>
              </div>
            
          </div>
           <div className="col-md-6">
               <div className="img-fluid">
                   <img src="assets/images/careers-2.png" className="img-fluid" />
               </div>
          </div>
          
      </div>
      
      <div className="row mb-4">
            <h2 className="section-title p-tb-20 text-center"> Teams</h2>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Sustainability</h5>
                    <p>8 Openings</p>
                    
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Energy</h5>
                    <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Environment</h5>
                     <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>GHG</h5>
                    <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Customer support</h5>
                     <p>8 Openings</p>
                </div>
            </div>
            <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Marketing</h5>
                   <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Data</h5>
                   <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>General &Admin</h5>
                    <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Technical Support</h5>
                   <p>8 Openings</p>
                </div>
            </div>
             <div className="col-md-3">
                <div className="card teams-content">
                    <h5>Design</h5>
                    <p>8 Openings</p>
                </div>
            </div>
         
      </div>
      
      <div className="row mb-4">
           <h2 className="section-title p-tb-20 text-center">Welcome to Beyond Smart CitiesSustainability</h2>  
           
            <div className="col-md-6">
               <div className="img-fluid">
                   <img src="assets/images/about-2.jpg" className="img-fluid careers-img" />
               </div>
          </div>
                <div className="col-md-6">
                 <h2 className="section-title p-tb-20 text-center">Get know Sustainability</h2>
                         <p>We provide Strategic Sustainability Assessment services and a range of services to support with the identif ication and implementation of sustainability and climate change solutions for master plans and projects. Sustainable services refer to an intention to minimize the negative impact of construction on the environment. Energy eff iciency plays a large role in this effort, and is Building commissioning’ primary focus within the process.</p>
          </div>
          
          
      </div>
      
      <div className="row mb-4">
             <h2 className="section-title p-tb-20 text-center"> Bengaluru Sustainability Development Team Specialist (Bengaluru)</h2>  
      
             <div className="collapsible-list">
                    <button type="button" className="collapsible">The Position</button>
          <div className="content"> 

                <div className="smart-list">
                    <p>BSC is looking for a Consultant to join our Built Energy and Sustainability Team based at our office in Bengaluru.Our Energy and Sustainability Team provide a range of assessments (Energy Reports, Overheating Assessments, Daylight Analysis, BREEAM Assessments, Sustainability Statements) on new, existing and operational buildings to a multitude of clients. RPS specialises in all areas of the built and natural environment, shaping the future of our environmental, social and economic landscapes.</p> 
              </div>
             </div>
              <button type="button" className="collapsible">Responsibilities</button>
          <div className="content"> 

                <div className="smart-list">
                   <p>You’ll be working with our award-winning team of Energy and BREEAM assessors and have responsibility for: the completion of energy calculations and sustainability assessments (e.g., BREEAM, Home Quality Mark, Sustainability reports etc) , data interpretation and analysis, site visits and surveys, regulatory consultations and report writing; delivery of Energy and Sustainability assessments on new, existing and operational buildings, site visits and surveys, report writing and reviewing both on a technical and commercial level.</p>
             </div>
             </div>
                <button type="button" className="collapsible">Requirements</button>
          <div className="content"> 

                <div className="smart-list">
             <p>You’ll have worked in a similar role and have experience of how the BREEAM process works and what the BRE require is essential.Knowledge of Code for Sustainable Homes, Life Cycle Assessments, Circular Economy, Home Quality Mark, WELL, Fitwel, LEED, CEEQUAL is beneficial.</p>
             </div>
             </div>
                <button type="button" className="collapsible">Benefits </button>
          <div className="content"> 

                <div className="smart-list">
                  <p>When you join our team, you're helping create opportunities for anyone in the world to build their business, brand, or dream. Is there a more meaningful benefit than that? To top it off we also have a tight-knit BSC culture, team gatherings, you will be able to work from a pretty amazing office-space</p>
             </div>
             </div>
             
             
             
             
             </div>
      </div>
      <div className="row mb-4">
          <div className="col-md-6">
               <h2 className="section-title p-tb-20 text-center"> All Open Positions</h2>
           <h4><b>Sustainability Specialist </b></h4>
           <p>        Beyond Smart Cities looking for.a Backend Sustainability <br/> Specialist Leader to join our team in Bengaluru</p>
          <button> <a href="">Read More</a></button>
        
          </div>
           <div className="col-md-6 careers-from">
               
                <h2 className="section-title p-tb-20 text-center"> Apply for this position</h2>
             
                 <form className="user-form" action="/" method="POST" enctype="">
                            
                            <div className="form-group">
                                <label>First Name*</label>
                                <input className="form-control" type="text" name="name" required="" placeholder=" Name" />
                                                                </div>
                                                                  <div className="form-group">
                                <label>Last name*</label>
                                <input className="form-control" type="text" name="name" required="" placeholder=" Name" />
                                                                </div>
                                                                
                                                              
                            <div className="form-group">
                              <label>Email*</label>
                                <input className="form-control" type="email" name="email" required="" placeholder="Email" />
                                                                </div>
                           
                            <div className="form-group">
                                <label>Resume</label> <input type="file" id="myFile" name="filename" />
                                </div>
                             <div className="form-group">
                                  <label> LinkedIn Profile URL</label>  
                                <input className="form-control" type="email" name="" required="" placeholder="https://www.linkedin.com/in/krishnajipawar/" />
                                
                                
                                                                </div>
                            <div className="form-group">
                             
                                <input className="form-control" type="text" name="" required="" placeholder="How can we partner ?" />
                                                                </div>
                                                                
                                        <a href="">Please review our privacy practices </a>   <br/>
                                        <a href=""> Privacy policy: Beyond Smart Cities</a>
                          <div className="form-group">
                                <div className="col-12">
                                     <button><a href="#">Submit Application</a></button>
                                </div>
                         
                           
                          
                        
                  </div></form>
               </div>
          
      </div>
</div>
</div>
            </div>
        )
    }
}
