import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div>
                    <div  className="Nature-banner inner-page">
      <div className="container">
        
          <div className="row mt-4">
            
              <div className="col-md-6">
                  <h2 className="about-section-title text-center" >About us
                  
                  </h2>
               
                  
                   <img src="assets/images/Client.png"  alt="Beyond Smart Cities" className="img-fluid about-img " />
                   <div className="text-center">
                       <h5>KRISHNAJI PAWAR <br/></h5>
                   <p>CEO & FOUNDER</p>
                   </div>
                   
              </div>
               <div className="col-md-6 p-tb-20">
                   <p><b>SUSTAINABILITY, IT’S A LIFESTYLE.</b></p>
                   <p>Beyond Smart Cities envisions a world where safety and abundance exists for all people. We
                            acknowledge that everything is interconnected and that all actions must ensure that the
                        well-being of earth and humanity is protected. </p>
                        <p>We see people and planet living in harmony without compromising the future of both. Our
                        solution is to give the market exactly what it is required for and more, by pairing Expert
                      Knowledge, economic, environmental, and social vitality throughout the world to achieve
                more with less</p>
                 <p> <span>“Any fool can make things bigger, more complex, and more violent.
                  It takes a touch of genius – and a lot of courage – to move in the opposite direction.”</span><br/> <b style={{float:'right'}}>- Albert Einstein</b>   </p>
                  
          <p>Our mission is to imagine a sustainable future that works for populations and social progress.</p>
           <p>
We contribute differentiating, high-performance, and value-added solutions to our clients to
meet the challenges of a fast-changing world: restoring value to communities, tackling
environmental and sustainability issues, reinventing mobility, harnessing the digital
revolution, etc. A wide array of opportunities for us to shape a sustainable world, for the
benefit of present and future generations.</p>
 <p>We see your vision, and everything we do is an effort to help you make the connections that
will turn that vision into reality, by building your Virtual Talent Bench of trusted people</p>
              </div>
          </div>
          <br/>
            
     
<p><b>Decentralizing your operating and talent models the right way</b></p>
 <p>Beyond Smart Cities’ platform connects businesses with on-demand freelance talent
offering digital services in more than 800 categories, across 7 verticals including Green
Building Design, Energy Engineering, Environmental Consulting, GHG Accounting, Climate
Change ,Health & Safety & Commissioning Services.</p>

             <div className="text-center mb-4">
                 <button><a href="leadership.php">Leader ship</a></button>
             </div>

           </div>
    <section className="section-about">
        
                      <div className="row pb-4">
              <div className="col-md-6">
                
                         <img src="assets/images/about-3.jpg" />
           
              </div>
              <div className="col-md-6 text-center">
                  <div className="core-section">
                   <h2 className="section-title  text-center"> Core Values</h2>
                   <p>Our Mission is to advance the Green Building Technology
movement through best-in-class Education and
Encouragement</p>
               <div className="boxes">
                    <div className="box-content">
                      <img src="assets/images/icons/Quality of life.png" class="img-fluid" /> 
                    
                        <h4>Quality of Life</h4>
                        <p>Proactively create a healthy worklife balance and consistent
enjoyment in day-to-day life for our
family (TEAM) and our community.</p>
                    </div>
                    <div className="box-content">
                       <img src="assets/images/icons/best in class.png" className="img-fluid" /> 
                        <h4>Best-in-class</h4>
                        <p>Global notoriety as a trendsetter
and influencer in all that we do education, TECHNOLOGY, and
customer service.</p>
                    </div>
                   
                   
               </div>
                <div className="boxes">
                    <div className="box-content">
                     <img src="assets/images/icons/Positivity (2).png" className="img-fluid" />
                        <h4>Positivity</h4>
                        <p>Passionate practice of
optimism and environmental
stewardship in the present
about the future.</p>
                    </div>
                    <div className="box-content">
                      <img src="assets/images/icons/Trust (2).png" className="img-fluid" />
                        <h4>Trust</h4>
                        <p>Faith in each other, goodwill
towards our customers & clients,
and consistent integrity have
yielded longevity in business.</p>
                    </div>
                   
              
              </div>
          </div>
         </div>
         </div>
    </section>      
  <section className="stories text-center">
 <div className="container">

                 <h2 className="about-section-title text-center" >Our Services
                  
                  </h2>
      
      <p>Beyond Smart Cities is dedicated to advancing and catalyzing sustainability in India and other part of built environment.Beyond
Smart Cities is a major international company in the construction engineering & mobility services sectors whose unique global
service range encompasses Environmental & sustainability services, energy modeling, engineering and operation</p>
   <div className="row pt-4">
  
     <div className="col-md-4">
       <div className="card-r">
         <img src="assets/images/about-1.jpg" alt="Beyond Smart Cities" />
         <div className="services-box">
           <h4>Freelance Services</h4>
         
         <p>Beyond Smart Cities is the India’s work
marketplace, connecting millions of Green
Building Professionals ,Energy Engineer &
Environment consultants with independent talent
around the globe.</p>
          <button><a href="">Learn More</a></button>
         </div>
        </div>    
     </div>
     <div className="col-md-4">
       <div className="card-r">

         <img src="assets/images/about-2.jpg" alt="Beyond Smart Cities" />
         <div className="services-box">
         <h4>SmartPro Product Rating</h4>
         <p>Smart Product rating is to enable smart
products market transformation in India &
achieve international standards in green
products and technologies</p>
          <button><a href="">Learn More</a></button>
       </div>   
       </div> 
     </div>
     <div className="col-md-4">
       <div className="card-r">
         <img src="assets/images/about-4.jpg" alt="Beyond Smart Cities" />
         <div className="services-box">
             <h4>Smart Property Rating System</h4>
         <p>We're committed to transforming how our
buildings are designed, constructed and operated
through BSAS, because we believe that every
person deserves a better, more sustainable life.</p>
         <button><a href="">Learn More</a></button>
       </div>    
       </div>
     </div>
     </div>
  
   </div>
 
</section>
         
           
    </div>
    

            </div>
        )
    }
}
