import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Footer extends Component{
    render(){
        return(
            <div>
               <section className="footer-section">
    <div className="container-fluid">

       <h2>KNOWLEDGE IS POWER</h2>
       <h5>Find the Talent needed to Get Your Business Growing</h5>
   <div className="row footer-content">
       <div className="col-md-3">
       <h4>Categories </h4>
           
                   <ul>
                        <li><Link to="/sustainability">Sustainability</Link></li>
                         <li>  <Link to="/smartcities">Smart Citiesy</Link> </li>
                          <li><Link to="/buildingtech">Building Tech</Link></li>
                           <li> <Link to="/energycxa">Energy &amp; CXA</Link> </li>
                            <li>    <Link to="/ghgclimatechange">GHG &amp; Climate Change</Link> </li>
                             <li> <Link to="/hsefiresafety">HSE &amp; Fire
Safety</Link> </li>
                              <li>  <Link to="/emergingtech">Emerging Tech</Link>
                     </li>
                                 <li><Link to="/smartproproductratingsystems">SmartPro Product Rating Systems</Link> </li>
                               <li><Link to="/bsasrating">BSAS Rating Systems</Link> </li>
                                  <li><Link to="/sitemap">Site Map</Link> </li>
                                 
           
                             </ul>
               
       </div>
       <div className="col-md-2">

           <h4>For Clients</h4>
           <ul>
            
              <li><Link to="/howtohire"> How to Hire</Link>  </li>
                    <li><Link to="/talentmarketplace">Talent Marketplace</Link>  </li>
                      <li><a href="">Find smart Products</a> </li>
                        <li><Link to="/selfcertification">Self Certification of property </Link></li>
                      <li><Link to="/propertyresources">Self Certification of property Resources</Link></li>
                          <li><Link to="/hireinworld">Hire in worldwide </Link></li>
                           <li><Link to="/hiremiddleeast">Hire in the Middle East</Link> </li>
                             <li><Link to="/greenbuildingtools">Green Building Tools</Link> </li>
                          
           </ul>
         </div> 
         <div className="col-md-2">
         <h4>For Genius</h4>
         <ul>
                 <li> <Link to="/howtofindwork">How to Find Work </Link></li>
                     
                        <li><Link to="/freelanceworldwide">Find Freelance Jobs Worldwide </Link></li>
                       
                          <li><Link to="/freelancegcc">Find Freelance Jobs in the GCC</Link></li>
                      
           </ul>

                </div>
                <div className="col-md-2">
                    <h4>Resources</h4>
                <ul>
            {/* <!--<li><a href="help-support.php">Help & Support</a> </li>--> */}
                        <li><a href="">Selling on Smartwork</a> </li>
                          <li><a href="">Buying on Smartwork</a> </li>
              <li>    <Link to="/articles">Articles</Link> </li>
                        <li><Link to="/bswslevelsystem">BSW’s level system</Link> </li>
                       
                    <li><Link to="/BSAScertifiedproject"> BSAS Certified project</Link></li>
                   
           </ul>
                </div>
                <div className="col-md-3">
                <h4>Company</h4>
                 <ul>
                     <li>
                       <Link to="/about">About Us</Link>
                     
                      
                    </li>
                     <li>
                     
                       <Link to="/leadership">Leadership</Link>
                       </li>

                      
                     <li>
                     <Link to="/InvestorRelations">Investor Relations</Link>
                      </li>
                     
                  
                     <li>
                      
                       <Link to="/login">Login</Link>
                       </li>
                     <li>
                      
                       <Link to="/beyondbusiness">Beyond Business</Link>
                       </li>
                     <li>
                     <Link to="/trustsafety">Trust & safety</Link>
                       
                       </li>
                     <li>    <Link to="/contact">Contact</Link></li>
                     
                      <li> <Link to="#">News</Link></li>
                    
              
                    
         
           
        
            
          
               
            
          
       </ul>
           </div>
      
      
   </div>
  
   </div>
  
</section>
<div className="row copyright">
    <div className="col-md-4 dash">
    <Link to="/termsservice">Terms of Service</Link>

     <Link to="/privacypolicy">Privacy Policy</Link>
    </div>
    <div className="col-md-4 dash">
          <p className="text-center"> © Beyond Smart Cities Private Limited - 2021</p>
  
    </div>
    <div className="col-md-4 dash">
       <ul className="footer-links">
		<li><a href="https://www.facebook.com/beyondsmartcities" target="_blank"><i className="fab fa-facebook"></i></a></li>
		<li><a href="https://www.instagram.com/beyond_smart_cities/" target="_blank"><i className="fab fa-instagram"></i></a></li>
		<li><a href="https://twitter.com/beyondsmartcity" target="_blank"><i className="fab fa-twitter"></i></a></li>
		<li><a href="https://www.linkedin.com/company/beyond-smart-cities/" target="_blank"><i className="fab fa-linkedin"></i></a></li>
		<li><a target="_blank" href="https://www.youtube.com/channel/UCUtIOMV7VbhzDA_wktBiBkg"><i className="fab fa-youtube"></i></a></li>
		</ul>
           </div>
</div>
            </div>
        )
    }
}


export default Footer;