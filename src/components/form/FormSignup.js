import React, {useState} from 'react'
import {Link} from 'react-router-dom';
import validate from './ValidateInfo';
import useForm from './UseForm';
const FormSignup = ({ submitForm }) => {

  
  
  const { handleChange, handleFormSubmit, values, errors } = useForm(validate);

    return (
        <div>
           <section class="user-area log-in-area">
        <div class="container">
            <div class="user-form-content">

                <div class="row">
                   

                    <div class="sign-layout">
                        <div class="login-form-holder position-relative">
                            <div class="form-group text-center">
                                <img src="assets/images/logo.png" alt=""  />

                                <h4 class="mt-4">Sign up</h4>

                                
                            </div>
                            <form  class="user-form" action="" method="POST" onSubmit={handleFormSubmit} >
                                
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" 
                                    type="text" 
                                    name="name" 
                                    id="name" value={values.name}
                                    onChange={handleChange}
                                    placeholder="Enter your name"
                                   />
                                     {errors.name && <p>{errors.name}</p>}
                                                                    </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" 
                                    type="email" 
                                    name="email" 
                                    id="email" value={values.email} 
                                    onChange={handleChange}
                                    placeholder="Enter your Email"
                                    />
                                        {errors.email && <p>{errors.email}</p>}
                                                                    </div>
                        
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" 
                                    type="password" 
                                    name="password" 
                                    id="password" 
                                    value={values.password}
                                    onChange={handleChange}
                                    placeholder="Enter password"
                                    
                                    />
                                      {errors.password && <p>{errors.password}</p>}
                                          </div>  
                                          <div class="form-group">                  
                                    <label>Repeat password</label>
                                    <input class="form-control" 
                                    type="password" 
                                    name="confirmation" 
                                    id="confirmation"
                                     value={values.confirmation}
                                    onChange={handleChange}
                                    placeholder="Re-enter password"
                                    />
                                     {errors.confirmation && <p>{errors.confirmation}</p>}
                                </div>
                                <div class="form-group mb-4">
                                    <label for="agree">
                                        <input type="checkbox" 
                                        name="checkbox" 
                                        value="check" 
                                        id="agree" 
                                    
                                      
                                        /> I
                                        agree to the <Link to="/termsservice"  class="text-color-orange" target="_blank">Terms and Conditions</Link> and <Link to="/privacypolicy" class="text-color-orange" target="_blank">Privacy
                                            Policy</Link>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary default-btn se-primary-btn"  type="submit">
                                        Register now
                                    </button>
                                </div>
                                <div class="form-group">
                                    <div class="col-12">
                                        <p class="create">Already have an account?<Link to="/login" className="text-color-orange"></Link>Log
                                                In</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
    </div></div></div></section>
          
        </div>
    )
}

export default FormSignup
